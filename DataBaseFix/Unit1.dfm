object Form1: TForm1
  Left = 405
  Top = 251
  BorderStyle = bsSingle
  Caption = 'Form1'
  ClientHeight = 183
  ClientWidth = 296
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 8
    Top = 8
    Width = 137
    Height = 169
    Caption = 'Mashin'
    TabOrder = 0
    object Button1: TButton
      Left = 32
      Top = 128
      Width = 75
      Height = 25
      Caption = 'Go!'
      TabOrder = 0
      OnClick = Button1Click
    end
    object Memo1: TMemo
      Left = 24
      Top = 48
      Width = 97
      Height = 73
      BevelInner = bvNone
      BevelOuter = bvNone
      BorderStyle = bsNone
      Color = clBtnFace
      Enabled = False
      Lines.Strings = (
        'Change Order Of '
        'Mashin DataBase '
        'Date')
      TabOrder = 1
    end
  end
  object GroupBox2: TGroupBox
    Left = 152
    Top = 8
    Width = 137
    Height = 169
    Caption = 'Motor'
    TabOrder = 1
    object Button2: TButton
      Left = 30
      Top = 128
      Width = 75
      Height = 25
      Caption = 'Go!'
      TabOrder = 0
      OnClick = Button2Click
    end
    object Memo2: TMemo
      Left = 24
      Top = 48
      Width = 97
      Height = 73
      BevelInner = bvNone
      BevelOuter = bvNone
      BorderStyle = bsNone
      Color = clBtnFace
      Enabled = False
      Lines.Strings = (
        'Change Order Of '
        'Motor DataBase '
        'Date')
      TabOrder = 1
    end
  end
  object Mashin: TTable
    TableName = 'MashinTable.DB'
    Left = 81
    Top = 35
    object MashinCodeBrgeZard: TStringField
      FieldName = 'CodeBrgeZard'
      Size = 255
    end
    object MashinNoo: TStringField
      FieldName = 'Noo'
      Size = 50
    end
    object MashinRang: TStringField
      FieldName = 'Rang'
      Size = 50
    end
    object MashinTarikhVorod: TStringField
      FieldName = 'TarikhVorod'
      Size = 10
    end
    object MashinShomarePelak: TStringField
      FieldName = 'ShomarePelak'
      Size = 25
    end
    object MashinShomareBadane: TStringField
      FieldName = 'ShomareBadane'
      Size = 50
    end
    object MashinShomareMotor: TStringField
      FieldName = 'ShomareMotor'
      Size = 50
    end
    object MashinYegan: TStringField
      FieldName = 'Yegan'
      Size = 80
    end
    object MashinAnbar: TStringField
      FieldName = 'Anbar'
      Size = 80
    end
    object MashinRadif: TSmallintField
      FieldName = 'Radif'
    end
    object MashinTarikhTarkhis: TStringField
      FieldName = 'TarikhTarkhis'
      Size = 10
    end
    object MashinHazineDaryafti: TFloatField
      FieldName = 'HazineDaryafti'
    end
    object MashinNooeZabt: TStringField
      FieldName = 'NooeZabt'
      Size = 25
    end
    object MashinVSSwitch: TBooleanField
      FieldName = 'VSSwitch'
    end
    object MashinVSFandak: TBooleanField
      FieldName = 'VSFandak'
    end
    object MashinVSJak: TBooleanField
      FieldName = 'VSJak'
    end
    object MashinVSAchaCharkh: TBooleanField
      FieldName = 'VSAchaCharkh'
    end
    object MashinVSAinehDakhel: TBooleanField
      FieldName = 'VSAinehDakhel'
    end
    object MashinVSZapas: TBooleanField
      FieldName = 'VSZapas'
    end
    object MashinVSAinehBagal: TBooleanField
      FieldName = 'VSAinehBagal'
    end
    object MashinVSGalpagh: TBooleanField
      FieldName = 'VSGalpagh'
    end
    object MashinVSRadioPakhsh: TBooleanField
      FieldName = 'VSRadioPakhsh'
    end
    object MashinVSProjektor: TBooleanField
      FieldName = 'VSProjektor'
    end
    object MashinVSBolandgoo: TBooleanField
      FieldName = 'VSBolandgoo'
    end
    object MashinVSCapsol: TBooleanField
      FieldName = 'VSCapsol'
    end
    object MashinVSGoflVaZanjir: TBooleanField
      FieldName = 'VSGoflVaZanjir'
    end
    object MashinTozihat: TStringField
      FieldName = 'Tozihat'
      Size = 255
    end
  end
  object DataSource1: TDataSource
    DataSet = Mashin
    Left = 109
    Top = 35
  end
  object MotorDataSource1: TDataSource
    DataSet = Motor
    Left = 237
    Top = 35
  end
  object Motor: TTable
    TableName = 'MotorTable.DB'
    Left = 209
    Top = 35
    object MotorCodeBrgeZard: TStringField
      FieldName = 'CodeBrgeZard'
      Size = 255
    end
    object MotorNoo: TStringField
      FieldName = 'Noo'
      Size = 50
    end
    object MotorRang: TStringField
      FieldName = 'Rang'
      Size = 50
    end
    object MotorTarikhVorod: TStringField
      FieldName = 'TarikhVorod'
      Size = 10
    end
    object MotorShomarePelak: TStringField
      FieldName = 'ShomarePelak'
      Size = 25
    end
    object MotorShomareBadane: TStringField
      FieldName = 'ShomareBadane'
      Size = 50
    end
    object MotorShomareMotor: TStringField
      FieldName = 'ShomareMotor'
      Size = 50
    end
    object MotorYegan: TStringField
      FieldName = 'Yegan'
      Size = 80
    end
    object MotorAnbar: TStringField
      FieldName = 'Anbar'
      Size = 80
    end
    object MotorRadif: TSmallintField
      FieldName = 'Radif'
    end
    object MotorTarikhTarkhis: TStringField
      FieldName = 'TarikhTarkhis'
      Size = 10
    end
    object MotorHazineDaryafti: TFloatField
      FieldName = 'HazineDaryafti'
    end
    object MotorNooeZabt: TStringField
      FieldName = 'NooeZabt'
      Size = 25
    end
    object MotorVSJak: TBooleanField
      FieldName = 'VSJak'
    end
    object MotorVSGabZanjir: TBooleanField
      FieldName = 'VSGabZanjir'
    end
    object MotorVSRecabJelo: TBooleanField
      FieldName = 'VSRecabJelo'
    end
    object MotorVSHendel: TBooleanField
      FieldName = 'VSHendel'
    end
    object MotorVSAchar: TBooleanField
      FieldName = 'VSAchar'
    end
    object MotorVSBatri: TBooleanField
      FieldName = 'VSBatri'
    end
    object MotorVSBaak: TBooleanField
      FieldName = 'VSBaak'
    end
    object MotorVSGabBagalDotarf: TBooleanField
      FieldName = 'VSGabBagalDotarf'
    end
    object MotorVSDasteTormoz: TBooleanField
      FieldName = 'VSDasteTormoz'
    end
    object MotorVSDasteKelaj: TBooleanField
      FieldName = 'VSDasteKelaj'
    end
    object MotorVSKaseKilometr: TBooleanField
      FieldName = 'VSKaseKilometr'
    end
    object MotorVSKaseDoreMotor: TBooleanField
      FieldName = 'VSKaseDoreMotor'
    end
    object MotorVSCherageJelo: TBooleanField
      FieldName = 'VSCherageJelo'
    end
    object MotorVSCherageAgab: TBooleanField
      FieldName = 'VSCherageAgab'
    end
    object MotorVSCherageRahnamaChap: TBooleanField
      FieldName = 'VSCherageRahnamaChap'
    end
    object MotorVSCherageRahnamaRast: TBooleanField
      FieldName = 'VSCherageRahnamaRast'
    end
    object MotorVSAineh: TBooleanField
      FieldName = 'VSAineh'
    end
    object MotorVSToshak: TBooleanField
      FieldName = 'VSToshak'
    end
    object MotorVSArmeBadaneh: TBooleanField
      FieldName = 'VSArmeBadaneh'
    end
    object MotorVSBoog: TBooleanField
      FieldName = 'VSBoog'
    end
    object MotorVSGofVaZanjir: TBooleanField
      FieldName = 'VSGofVaZanjir'
    end
    object MotorVSTalg: TBooleanField
      FieldName = 'VSTalg'
    end
    object MotorVSSwitch: TBooleanField
      FieldName = 'VSSwitch'
    end
    object MotorTozihat: TStringField
      FieldName = 'Tozihat'
      Size = 255
    end
  end
end

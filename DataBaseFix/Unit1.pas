unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Mask, DBCtrls, DB, DBTables;

type
  TForm1 = class(TForm)
    GroupBox1: TGroupBox;
    Mashin: TTable;
    MashinCodeBrgeZard: TStringField;
    MashinNoo: TStringField;
    MashinRang: TStringField;
    MashinTarikhVorod: TStringField;
    MashinShomarePelak: TStringField;
    MashinShomareBadane: TStringField;
    MashinShomareMotor: TStringField;
    MashinYegan: TStringField;
    MashinAnbar: TStringField;
    MashinRadif: TSmallintField;
    MashinTarikhTarkhis: TStringField;
    MashinHazineDaryafti: TFloatField;
    MashinNooeZabt: TStringField;
    MashinVSSwitch: TBooleanField;
    MashinVSFandak: TBooleanField;
    MashinVSJak: TBooleanField;
    MashinVSAchaCharkh: TBooleanField;
    MashinVSAinehDakhel: TBooleanField;
    MashinVSZapas: TBooleanField;
    MashinVSAinehBagal: TBooleanField;
    MashinVSGalpagh: TBooleanField;
    MashinVSRadioPakhsh: TBooleanField;
    MashinVSProjektor: TBooleanField;
    MashinVSBolandgoo: TBooleanField;
    MashinVSCapsol: TBooleanField;
    MashinVSGoflVaZanjir: TBooleanField;
    MashinTozihat: TStringField;
    DataSource1: TDataSource;
    MotorDataSource1: TDataSource;
    Motor: TTable;
    MotorCodeBrgeZard: TStringField;
    MotorNoo: TStringField;
    MotorRang: TStringField;
    MotorTarikhVorod: TStringField;
    MotorShomarePelak: TStringField;
    MotorShomareBadane: TStringField;
    MotorShomareMotor: TStringField;
    MotorYegan: TStringField;
    MotorAnbar: TStringField;
    MotorRadif: TSmallintField;
    MotorTarikhTarkhis: TStringField;
    MotorHazineDaryafti: TFloatField;
    MotorNooeZabt: TStringField;
    MotorVSJak: TBooleanField;
    MotorVSGabZanjir: TBooleanField;
    MotorVSRecabJelo: TBooleanField;
    MotorVSHendel: TBooleanField;
    MotorVSAchar: TBooleanField;
    MotorVSBatri: TBooleanField;
    MotorVSBaak: TBooleanField;
    MotorVSGabBagalDotarf: TBooleanField;
    MotorVSDasteTormoz: TBooleanField;
    MotorVSDasteKelaj: TBooleanField;
    MotorVSKaseKilometr: TBooleanField;
    MotorVSKaseDoreMotor: TBooleanField;
    MotorVSCherageJelo: TBooleanField;
    MotorVSCherageAgab: TBooleanField;
    MotorVSCherageRahnamaChap: TBooleanField;
    MotorVSCherageRahnamaRast: TBooleanField;
    MotorVSAineh: TBooleanField;
    MotorVSToshak: TBooleanField;
    MotorVSArmeBadaneh: TBooleanField;
    MotorVSBoog: TBooleanField;
    MotorVSGofVaZanjir: TBooleanField;
    MotorVSTalg: TBooleanField;
    MotorVSSwitch: TBooleanField;
    MotorTozihat: TStringField;
    GroupBox2: TGroupBox;
    Button1: TButton;
    Button2: TButton;
    Memo1: TMemo;
    Memo2: TMemo;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    function Dorost(inp:string):string;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
var
i:integer;
begin
Mashin.Active := true;
Mashin.First;

for i := 0 to mashin.RecordCount-1 do begin
  mashin.Edit;

  mashin.Fields[3].AsString := Dorost(mashin.Fields[3].AsString);
  mashin.Fields[10].AsString := Dorost(mashin.Fields[10].AsString);

  mashin.Post;  
  mashin.Next;
end;

Mashin.Active := false;
showmessage('Done!');
end;

function TForm1.Dorost(inp: string): string;
var
Year, Month, Day:string;
begin
//  inp           out
//2/24/1385 -> 1385/2/24

if inp='' then begin
  result := '';
  exit;
end;

Month := copy(inp, 1, pos('/', inp)-1);
inp := copy(inp, pos('/', inp)+1, length(inp));

Day := copy(inp, 1, pos('/', inp)-1);
inp := copy(inp, pos('/', inp)+1, length(inp));

Year := copy(inp, 1,length(inp));
inp := copy(inp, pos('/', inp)+1, length(inp));

result := Year+'/'+Month+'/'+Day;
end;

procedure TForm1.Button2Click(Sender: TObject);
var
i:integer;
begin
motor.Active := true;
motor.First;

for i := 0 to motor.RecordCount-1 do begin
  motor.Edit;

  motor.Fields[3].AsString := Dorost(motor.Fields[3].AsString);
  motor.Fields[10].AsString := Dorost(motor.Fields[10].AsString);

  motor.Post;
  motor.Next;
end;

Motor.Active := false;
showmessage('Done!');
end;

end.

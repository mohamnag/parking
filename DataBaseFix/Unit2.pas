unit Unit2;

interface

uses
  Windows, Messages, Classes, SysUtils, Graphics, Controls, StdCtrls, Forms,
  Dialogs, DBCtrls, DB, DBTables, Mask, ExtCtrls;

type
  TForm2 = class(TForm)
    MashinCodeBrgeZard: TStringField;
    MashinNoo: TStringField;
    MashinRang: TStringField;
    MashinTarikhVorod: TStringField;
    MashinShomarePelak: TStringField;
    MashinShomareBadane: TStringField;
    MashinShomareMotor: TStringField;
    MashinYegan: TStringField;
    MashinAnbar: TStringField;
    MashinRadif: TSmallintField;
    MashinTarikhTarkhis: TStringField;
    MashinHazineDaryafti: TFloatField;
    MashinNooeZabt: TStringField;
    MashinVSSwitch: TBooleanField;
    MashinVSFandak: TBooleanField;
    MashinVSJak: TBooleanField;
    MashinVSAchaCharkh: TBooleanField;
    MashinVSAinehDakhel: TBooleanField;
    MashinVSZapas: TBooleanField;
    MashinVSAinehBagal: TBooleanField;
    MashinVSGalpagh: TBooleanField;
    MashinVSRadioPakhsh: TBooleanField;
    MashinVSProjektor: TBooleanField;
    MashinVSBolandgoo: TBooleanField;
    MashinVSCapsol: TBooleanField;
    MashinVSGoflVaZanjir: TBooleanField;
    MashinTozihat: TStringField;
    ScrollBox: TScrollBox;
    Label1: TLabel;
    EditCodeBrgeZard: TDBEdit;
    Label2: TLabel;
    EditNoo: TDBEdit;
    Label3: TLabel;
    EditRang: TDBEdit;
    Label4: TLabel;
    EditTarikhVorod: TDBEdit;
    Label5: TLabel;
    EditShomarePelak: TDBEdit;
    Label6: TLabel;
    EditShomareBadane: TDBEdit;
    Label7: TLabel;
    EditShomareMotor: TDBEdit;
    Label8: TLabel;
    EditYegan: TDBEdit;
    Label9: TLabel;
    EditAnbar: TDBEdit;
    Label10: TLabel;
    EditRadif: TDBEdit;
    Label11: TLabel;
    EditTarikhTarkhis: TDBEdit;
    Label12: TLabel;
    EditHazineDaryafti: TDBEdit;
    Label13: TLabel;
    EditNooeZabt: TDBEdit;
    Label14: TLabel;
    EditVSSwitch: TDBEdit;
    Label15: TLabel;
    EditVSFandak: TDBEdit;
    Label16: TLabel;
    EditVSJak: TDBEdit;
    Label17: TLabel;
    EditVSAchaCharkh: TDBEdit;
    Label18: TLabel;
    EditVSAinehDakhel: TDBEdit;
    Label19: TLabel;
    EditVSZapas: TDBEdit;
    Label20: TLabel;
    EditVSAinehBagal: TDBEdit;
    Label21: TLabel;
    EditVSGalpagh: TDBEdit;
    Label22: TLabel;
    EditVSRadioPakhsh: TDBEdit;
    Label23: TLabel;
    EditVSProjektor: TDBEdit;
    Label24: TLabel;
    EditVSBolandgoo: TDBEdit;
    Label25: TLabel;
    EditVSCapsol: TDBEdit;
    Label26: TLabel;
    EditVSGoflVaZanjir: TDBEdit;
    Label27: TLabel;
    EditTozihat: TDBEdit;
    DBNavigator: TDBNavigator;
    Panel1: TPanel;
    DataSource1: TDataSource;
    Panel2: TPanel;
    Mashin: TTable;
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  Form2: TForm2;

implementation

{$R *.DFM}

end.

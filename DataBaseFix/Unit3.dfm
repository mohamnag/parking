�
 TFORM3 0�*  TPF0TForm3Form3Left� TopvWidth�HeightxActiveControlPanel1CaptionForm3Color	clBtnFace
ParentFont	OldCreateOrder	PositionpoScreenCenterPixelsPerInch`
TextHeight TPanelPanel1Left Top Width�Height!AlignalTop
BevelOuterbvNoneTabOrder  TDBNavigatorDBNavigatorLeftTopWidth� Height
DataSourceMotorDataSource1Flat	Ctl3DParentCtl3DTabOrder    TPanelPanel2Left Top!Width�Height5AlignalClient
BevelOuterbvNoneBorderWidthCaptionPanel2TabOrder 
TScrollBox	ScrollBoxLeftTopWidth�Height-HorzScrollBar.MarginHorzScrollBar.Range�VertScrollBar.MarginVertScrollBar.Range�AlignalClient
AutoScrollBorderStylebsNoneTabOrder  TLabelLabel1LeftTopWidthEHeightCaptionCodeBrgeZardFocusControlEditCodeBrgeZard  TLabelLabel2LeftTop,WidthHeightCaptionNooFocusControlEditNoo  TLabelLabel3LeftTopRWidthHeightCaptionRangFocusControlEditRang  TLabelLabel4LeftTopRWidth:HeightCaptionTarikhVorodFocusControlEditTarikhVorod  TLabelLabel5LeftTopxWidthEHeightCaptionShomarePelakFocusControlEditShomarePelak  TLabelLabel6Left� TopxWidthOHeightCaptionShomareBadaneFocusControlEditShomareBadane  TLabelLabel7LeftTop� WidthEHeightCaptionShomareMotorFocusControlEditShomareMotor  TLabelLabel8LeftTop� WidthHeightCaptionYeganFocusControl	EditYegan  TLabelLabel9LeftTop� WidthHeightCaptionAnbarFocusControl	EditAnbar  TLabelLabel10Left,Top� WidthHeightCaptionRadifFocusControl	EditRadif  TLabelLabel11LeftiTop� WidthAHeightCaptionTarikhTarkhisFocusControlEditTarikhTarkhis  TLabelLabel12LeftTopWidthEHeightCaptionHazineDaryaftiFocusControlEditHazineDaryafti  TLabelLabel13LeftQTopWidth0HeightCaptionNooeZabtFocusControlEditNooeZabt  TLabelLabel14Left� TopWidthHeightCaptionVSJakFocusControl	EditVSJak  TLabelLabel15LeftTopWidth<HeightCaptionVSGabZanjirFocusControlEditVSGabZanjir  TLabelLabel16LeftXTopWidthAHeightCaptionVSRecabJeloFocusControlEditVSRecabJelo  TLabelLabel17LeftTop6Width0HeightCaptionVSHendelFocusControlEditVSHendel  TLabelLabel18Left<Top6Width*HeightCaptionVSAcharFocusControlEditVSAchar  TLabelLabel19LeftoTop6Width#HeightCaptionVSBatriFocusControlEditVSBatri  TLabelLabel20Left� Top6Width'HeightCaptionVSBaakFocusControl
EditVSBaak  TLabelLabel21Left� Top6WidthZHeightCaptionVSGabBagalDotarfFocusControlEditVSGabBagalDotarf  TLabelLabel22Left5Top6WidthMHeightCaptionVSDasteTormozFocusControlEditVSDasteTormoz  TLabelLabel23LeftTop\WidthAHeightCaptionVSDasteKelajFocusControlEditVSDasteKelaj  TLabelLabel24LeftMTop\WidthKHeightCaptionVSKaseKilometrFocusControlEditVSKaseKilometr  TLabelLabel25Left� Top\WidthXHeightCaptionVSKaseDoreMotorFocusControlEditVSKaseDoreMotor  TLabelLabel26Left� Top\WidthIHeightCaptionVSCherageJeloFocusControlEditVSCherageJelo  TLabelLabel27LeftKTop\WidthOHeightCaptionVSCherageAgabFocusControlEditVSCherageAgab  TLabelLabel28LeftTop�Width}HeightCaptionVSCherageRahnamaChapFocusControlEditVSCherageRahnamaChap  TLabelLabel29Left� Top�WidthzHeightCaptionVSCherageRahnamaRastFocusControlEditVSCherageRahnamaRast  TLabelLabel30Left	Top�Width)HeightCaptionVSAinehFocusControlEditVSAineh  TLabelLabel31Left<Top�Width2HeightCaptionVSToshakFocusControlEditVSToshak  TLabelLabel32LeftTop�WidthQHeightCaptionVSArmeBadanehFocusControlEditVSArmeBadaneh  TLabelLabel33Left]Top�Width'HeightCaptionVSBoogFocusControl
EditVSBoog  TLabelLabel34Left� Top�WidthFHeightCaptionVSGofVaZanjirFocusControlEditVSGofVaZanjir  TLabelLabel35Left� Top�Width#HeightCaptionVSTalgFocusControl
EditVSTalg  TLabelLabel36LeftTop�Width.HeightCaptionVSSwitchFocusControlEditVSSwitch  TLabelLabel37LeftTop�Width#HeightCaptionTozihatFocusControlEditTozihat  TDBEditEditCodeBrgeZardLeftTopWidth Height	DataFieldCodeBrgeZard
DataSourceMotorDataSource1TabOrder   TDBEditEditNooLeftTop;Width	Height	DataFieldNoo
DataSourceMotorDataSource1TabOrder  TDBEditEditRangLeftTopaWidth	Height	DataFieldRang
DataSourceMotorDataSource1TabOrder  TDBEditEditTarikhVorodLeftTopaWidthAHeight	DataFieldTarikhVorod
DataSourceMotorDataSource1TabOrder  TDBEditEditShomarePelakLeftTop� Width� Height	DataFieldShomarePelak
DataSourceMotorDataSource1TabOrder  TDBEditEditShomareBadaneLeft� Top� Width	Height	DataFieldShomareBadane
DataSourceMotorDataSource1TabOrder  TDBEditEditShomareMotorLeftTop� Width	Height	DataFieldShomareMotor
DataSourceMotorDataSource1TabOrder  TDBEdit	EditYeganLeftTop� Width Height	DataFieldYegan
DataSourceMotorDataSource1TabOrder  TDBEdit	EditAnbarLeftTop� Width Height	DataFieldAnbar
DataSourceMotorDataSource1TabOrder  TDBEdit	EditRadifLeft,Top� Width7Height	DataFieldRadif
DataSourceMotorDataSource1TabOrder	  TDBEditEditTarikhTarkhisLeftiTop� WidthAHeight	DataFieldTarikhTarkhis
DataSourceMotorDataSource1TabOrder
  TDBEditEditHazineDaryaftiLeftTopWidthAHeight	DataFieldHazineDaryafti
DataSourceMotorDataSource1TabOrder  TDBEditEditNooeZabtLeftQTopWidth� Height	DataFieldNooeZabt
DataSourceMotorDataSource1TabOrder  TDBEdit	EditVSJakLeft� TopWidth-Height	DataFieldVSJak
DataSourceMotorDataSource1TabOrder  TDBEditEditVSGabZanjirLeftTopWidth-Height	DataFieldVSGabZanjir
DataSourceMotorDataSource1TabOrder  TDBEditEditVSRecabJeloLeftXTopWidth-Height	DataFieldVSRecabJelo
DataSourceMotorDataSource1TabOrder  TDBEditEditVSHendelLeftTopEWidth-Height	DataFieldVSHendel
DataSourceMotorDataSource1TabOrder  TDBEditEditVSAcharLeft<TopEWidth-Height	DataFieldVSAchar
DataSourceMotorDataSource1TabOrder  TDBEditEditVSBatriLeftoTopEWidth-Height	DataFieldVSBatri
DataSourceMotorDataSource1TabOrder  TDBEdit
EditVSBaakLeft� TopEWidth-Height	DataFieldVSBaak
DataSourceMotorDataSource1TabOrder  TDBEditEditVSGabBagalDotarfLeft� TopEWidth-Height	DataFieldVSGabBagalDotarf
DataSourceMotorDataSource1TabOrder  TDBEditEditVSDasteTormozLeft5TopEWidth-Height	DataFieldVSDasteTormoz
DataSourceMotorDataSource1TabOrder  TDBEditEditVSDasteKelajLeftTopkWidth-Height	DataFieldVSDasteKelaj
DataSourceMotorDataSource1TabOrder  TDBEditEditVSKaseKilometrLeftMTopkWidth-Height	DataFieldVSKaseKilometr
DataSourceMotorDataSource1TabOrder  TDBEditEditVSKaseDoreMotorLeft� TopkWidth-Height	DataFieldVSKaseDoreMotor
DataSourceMotorDataSource1TabOrder  TDBEditEditVSCherageJeloLeft� TopkWidth-Height	DataFieldVSCherageJelo
DataSourceMotorDataSource1TabOrder  TDBEditEditVSCherageAgabLeftKTopkWidth-Height	DataFieldVSCherageAgab
DataSourceMotorDataSource1TabOrder  TDBEditEditVSCherageRahnamaChapLeftTop�Width-Height	DataFieldVSCherageRahnamaChap
DataSourceMotorDataSource1TabOrder  TDBEditEditVSCherageRahnamaRastLeft� Top�Width-Height	DataFieldVSCherageRahnamaRast
DataSourceMotorDataSource1TabOrder  TDBEditEditVSAinehLeft	Top�Width-Height	DataFieldVSAineh
DataSourceMotorDataSource1TabOrder  TDBEditEditVSToshakLeft<Top�Width-Height	DataFieldVSToshak
DataSourceMotorDataSource1TabOrder  TDBEditEditVSArmeBadanehLeftTop�Width-Height	DataFieldVSArmeBadaneh
DataSourceMotorDataSource1TabOrder  TDBEdit
EditVSBoogLeft]Top�Width-Height	DataFieldVSBoog
DataSourceMotorDataSource1TabOrder   TDBEditEditVSGofVaZanjirLeft� Top�Width-Height	DataFieldVSGofVaZanjir
DataSourceMotorDataSource1TabOrder!  TDBEdit
EditVSTalgLeft� Top�Width-Height	DataFieldVSTalg
DataSourceMotorDataSource1TabOrder"  TDBEditEditVSSwitchLeftTop�Width-Height	DataFieldVSSwitch
DataSourceMotorDataSource1TabOrder#  TDBEditEditTozihatLeftTop�Width Height	DataFieldTozihat
DataSourceMotorDataSource1TabOrder$    TDataSourceMotorDataSource1DataSetMotorLeft�Top  TTableMotorDatabaseNameE:\Myprog\Parking	TableNameMotorTable.DBLeftiTop TStringFieldMotorCodeBrgeZard	FieldNameCodeBrgeZardSize�   TStringFieldMotorNoo	FieldNameNooSize2  TStringField	MotorRang	FieldNameRangSize2  TStringFieldMotorTarikhVorod	FieldNameTarikhVorodSize
  TStringFieldMotorShomarePelak	FieldNameShomarePelakSize  TStringFieldMotorShomareBadane	FieldNameShomareBadaneSize2  TStringFieldMotorShomareMotor	FieldNameShomareMotorSize2  TStringField
MotorYegan	FieldNameYeganSizeP  TStringField
MotorAnbar	FieldNameAnbarSizeP  TSmallintField
MotorRadif	FieldNameRadif  TStringFieldMotorTarikhTarkhis	FieldNameTarikhTarkhisSize
  TFloatFieldMotorHazineDaryafti	FieldNameHazineDaryafti  TStringFieldMotorNooeZabt	FieldNameNooeZabtSize  TBooleanField
MotorVSJak	FieldNameVSJak  TBooleanFieldMotorVSGabZanjir	FieldNameVSGabZanjir  TBooleanFieldMotorVSRecabJelo	FieldNameVSRecabJelo  TBooleanFieldMotorVSHendel	FieldNameVSHendel  TBooleanFieldMotorVSAchar	FieldNameVSAchar  TBooleanFieldMotorVSBatri	FieldNameVSBatri  TBooleanFieldMotorVSBaak	FieldNameVSBaak  TBooleanFieldMotorVSGabBagalDotarf	FieldNameVSGabBagalDotarf  TBooleanFieldMotorVSDasteTormoz	FieldNameVSDasteTormoz  TBooleanFieldMotorVSDasteKelaj	FieldNameVSDasteKelaj  TBooleanFieldMotorVSKaseKilometr	FieldNameVSKaseKilometr  TBooleanFieldMotorVSKaseDoreMotor	FieldNameVSKaseDoreMotor  TBooleanFieldMotorVSCherageJelo	FieldNameVSCherageJelo  TBooleanFieldMotorVSCherageAgab	FieldNameVSCherageAgab  TBooleanFieldMotorVSCherageRahnamaChap	FieldNameVSCherageRahnamaChap  TBooleanFieldMotorVSCherageRahnamaRast	FieldNameVSCherageRahnamaRast  TBooleanFieldMotorVSAineh	FieldNameVSAineh  TBooleanFieldMotorVSToshak	FieldNameVSToshak  TBooleanFieldMotorVSArmeBadaneh	FieldNameVSArmeBadaneh  TBooleanFieldMotorVSBoog	FieldNameVSBoog  TBooleanFieldMotorVSGofVaZanjir	FieldNameVSGofVaZanjir  TBooleanFieldMotorVSTalg	FieldNameVSTalg  TBooleanFieldMotorVSSwitch	FieldNameVSSwitch  TStringFieldMotorTozihat	FieldNameTozihatSize�     
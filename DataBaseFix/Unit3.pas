unit Unit3;

interface

uses
  Windows, Messages, Classes, SysUtils, Graphics, Controls, StdCtrls, Forms,
  Dialogs, DBCtrls, DB, DBTables, Mask, ExtCtrls;

type
  TForm3 = class(TForm)
    MotorCodeBrgeZard: TStringField;
    MotorNoo: TStringField;
    MotorRang: TStringField;
    MotorTarikhVorod: TStringField;
    MotorShomarePelak: TStringField;
    MotorShomareBadane: TStringField;
    MotorShomareMotor: TStringField;
    MotorYegan: TStringField;
    MotorAnbar: TStringField;
    MotorRadif: TSmallintField;
    MotorTarikhTarkhis: TStringField;
    MotorHazineDaryafti: TFloatField;
    MotorNooeZabt: TStringField;
    MotorVSJak: TBooleanField;
    MotorVSGabZanjir: TBooleanField;
    MotorVSRecabJelo: TBooleanField;
    MotorVSHendel: TBooleanField;
    MotorVSAchar: TBooleanField;
    MotorVSBatri: TBooleanField;
    MotorVSBaak: TBooleanField;
    MotorVSGabBagalDotarf: TBooleanField;
    MotorVSDasteTormoz: TBooleanField;
    MotorVSDasteKelaj: TBooleanField;
    MotorVSKaseKilometr: TBooleanField;
    MotorVSKaseDoreMotor: TBooleanField;
    MotorVSCherageJelo: TBooleanField;
    MotorVSCherageAgab: TBooleanField;
    MotorVSCherageRahnamaChap: TBooleanField;
    MotorVSCherageRahnamaRast: TBooleanField;
    MotorVSAineh: TBooleanField;
    MotorVSToshak: TBooleanField;
    MotorVSArmeBadaneh: TBooleanField;
    MotorVSBoog: TBooleanField;
    MotorVSGofVaZanjir: TBooleanField;
    MotorVSTalg: TBooleanField;
    MotorVSSwitch: TBooleanField;
    MotorTozihat: TStringField;
    ScrollBox: TScrollBox;
    Label1: TLabel;
    EditCodeBrgeZard: TDBEdit;
    Label2: TLabel;
    EditNoo: TDBEdit;
    Label3: TLabel;
    EditRang: TDBEdit;
    Label4: TLabel;
    EditTarikhVorod: TDBEdit;
    Label5: TLabel;
    EditShomarePelak: TDBEdit;
    Label6: TLabel;
    EditShomareBadane: TDBEdit;
    Label7: TLabel;
    EditShomareMotor: TDBEdit;
    Label8: TLabel;
    EditYegan: TDBEdit;
    Label9: TLabel;
    EditAnbar: TDBEdit;
    Label10: TLabel;
    EditRadif: TDBEdit;
    Label11: TLabel;
    EditTarikhTarkhis: TDBEdit;
    Label12: TLabel;
    EditHazineDaryafti: TDBEdit;
    Label13: TLabel;
    EditNooeZabt: TDBEdit;
    Label14: TLabel;
    EditVSJak: TDBEdit;
    Label15: TLabel;
    EditVSGabZanjir: TDBEdit;
    Label16: TLabel;
    EditVSRecabJelo: TDBEdit;
    Label17: TLabel;
    EditVSHendel: TDBEdit;
    Label18: TLabel;
    EditVSAchar: TDBEdit;
    Label19: TLabel;
    EditVSBatri: TDBEdit;
    Label20: TLabel;
    EditVSBaak: TDBEdit;
    Label21: TLabel;
    EditVSGabBagalDotarf: TDBEdit;
    Label22: TLabel;
    EditVSDasteTormoz: TDBEdit;
    Label23: TLabel;
    EditVSDasteKelaj: TDBEdit;
    Label24: TLabel;
    EditVSKaseKilometr: TDBEdit;
    Label25: TLabel;
    EditVSKaseDoreMotor: TDBEdit;
    Label26: TLabel;
    EditVSCherageJelo: TDBEdit;
    Label27: TLabel;
    EditVSCherageAgab: TDBEdit;
    Label28: TLabel;
    EditVSCherageRahnamaChap: TDBEdit;
    Label29: TLabel;
    EditVSCherageRahnamaRast: TDBEdit;
    Label30: TLabel;
    EditVSAineh: TDBEdit;
    Label31: TLabel;
    EditVSToshak: TDBEdit;
    Label32: TLabel;
    EditVSArmeBadaneh: TDBEdit;
    Label33: TLabel;
    EditVSBoog: TDBEdit;
    Label34: TLabel;
    EditVSGofVaZanjir: TDBEdit;
    Label35: TLabel;
    EditVSTalg: TDBEdit;
    Label36: TLabel;
    EditVSSwitch: TDBEdit;
    Label37: TLabel;
    EditTozihat: TDBEdit;
    DBNavigator: TDBNavigator;
    Panel1: TPanel;
    MotorDataSource1: TDataSource;
    Panel2: TPanel;
    Motor: TTable;
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  Form3: TForm3;

implementation

{$R *.DFM}

end.

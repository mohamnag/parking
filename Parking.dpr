program Parking;

uses
  Forms,
  Unit1 in 'Unit1.pas' {MainF},
  Unit2 in 'Unit2.pas' {Tarkhis},
  Unit3 in 'Unit3.pas' {ChkPassF},
  Unit4 in 'Unit4.pas' {MainPassF},
  Unit5 in 'Unit5.pas' {MMessageF},
  Unit6 in 'Unit6.pas' {StartF},
  Unit7 in 'Unit7.pas' {About},
  Unit8 in 'Unit8.pas' {TahvilBargeZardF},
  Unit9 in 'Unit9.pas' {NewMotorOfAjans},
  Unit10 in 'Unit10.pas' {NewMotAndMash},
  Unit11 in 'Unit11.pas' {ACForm},
  DateConv in 'DateConv.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TStartF, StartF);
  Application.CreateForm(TMainF, MainF);
  Application.CreateForm(TTarkhis, Tarkhis);
  Application.CreateForm(TChkPassF, ChkPassF);
  Application.CreateForm(TMainPassF, MainPassF);
  Application.CreateForm(TMMessageF, MMessageF);
  Application.CreateForm(TAbout, About);
  Application.CreateForm(TTahvilBargeZardF, TahvilBargeZardF);
  Application.CreateForm(TNewMotorOfAjans, NewMotorOfAjans);
  Application.CreateForm(TNewMotAndMash, NewMotAndMash);
  Application.CreateForm(TACForm, ACForm);
  Application.Run;
end.

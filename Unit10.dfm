object NewMotAndMash: TNewMotAndMash
  Left = 280
  Top = 200
  BiDiMode = bdRightToLeft
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = #1575#1591#1604#1575#1593#1575#1578
  ClientHeight = 272
  ClientWidth = 624
  Color = clBtnFace
  Font.Charset = ARABIC_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  ParentBiDiMode = False
  Position = poScreenCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 14
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 161
    Height = 272
    Align = alLeft
    TabOrder = 0
    object TntLabel11: TTntLabel
      Left = 1
      Top = 1
      Width = 159
      Height = 14
      Align = alTop
      Caption = ' '#1608#1587#1575#1740#1604' '#1607#1605#1585#1575#1607':'
      Caption_UTF7 = ' +BkgGMwYnBswGRA +BkcGRQYxBicGRw:'
    end
    object MotorVSList: TTntCheckListBox
      Left = 1
      Top = 15
      Width = 159
      Height = 256
      Align = alClient
      ItemHeight = 14
      Items.WideStrings = (
        #1580#1705
        #1602#1575#1576' '#1586#1606#1580#1610#1585
        #1585#1705#1575#1576' '#1580#1604#1608
        #1607#1606#1583#1604
        #1570#1670#1575#1585
        #1576#1575#1591#1585#1610
        #1576#1575#1705
        #1602#1575#1576' '#1576#1594#1604' '#1583#1608' '#1591#1585#1601
        #1583#1587#1578#1607' '#1578#1585#1605#1586
        #1583#1587#1578#1607' '#1705#1604#1575#1580
        #1705#1575#1587#1607' '#1705#1610#1604#1608#1605#1578#1585
        #1705#1575#1587#1607' '#1583#1608#1585' '#1605#1608#1578#1608#1585
        #1670#1585#1575#1594' '#1580#1604#1608
        #1670#1585#1575#1594' '#1593#1602#1576
        #1670#1585#1575#1594#1607#1575#1610' '#1585#1575#1607#1606#1605#1575' '#1670#1662
        #1670#1585#1575#1594#1607#1575#1610' '#1585#1575#1607#1606#1605#1575' '#1585#1575#1587#1578
        #1570#1574#1610#1606#1607
        #1578#1588#1705
        #1570#1585#1605#1607#1575#1610' '#1576#1583#1606#1607
        #1576#1608#1602
        #1602#1601#1604' '#1608' '#1586#1606#1580#1610#1585
        #1591#1604#1602
        #1587#1608#1574#1610#1670)
      Items.WideStrings_UTF7 = (
        '+BiwGqQ'
        '+BkIGJwYo +BjIGRgYsBkoGMQ'
        '+BjEGqQYnBig +BiwGRAZI'
        '+BkcGRgYvBkQ'
        '+BiIGhgYnBjE'
        '+BigGJwY3BjEGSg'
        '+BigGJwap'
        '+BkIGJwYo +BigGOgZE +Bi8GSA +BjcGMQZB'
        '+Bi8GMwYqBkc +BioGMQZFBjI'
        '+Bi8GMwYqBkc +BqkGRAYnBiw'
        '+BqkGJwYzBkc +BqkGSgZEBkgGRQYqBjE'
        '+BqkGJwYzBkc +Bi8GSAYx +BkUGSAYqBkgGMQ'
        '+BoYGMQYnBjo +BiwGRAZI'
        '+BoYGMQYnBjo +BjkGQgYo'
        '+BoYGMQYnBjoGRwYnBko +BjEGJwZHBkYGRQYn +BoYGfg'
        '+BoYGMQYnBjoGRwYnBko +BjEGJwZHBkYGRQYn +BjEGJwYzBio'
        '+BiIGJgZKBkYGRw'
        '+BioGNAap'
        '+BiIGMQZFBkcGJwZK +BigGLwZGBkc'
        '+BigGSAZC'
        '+BkIGQQZE +Bkg +BjIGRgYsBkoGMQ'
        '+BjcGRAZC'
        '+BjMGSAYmBkoGhg')
      TabOrder = 0
    end
    object MashinVSList: TTntCheckListBox
      Left = 1
      Top = 15
      Width = 159
      Height = 256
      Align = alClient
      ItemHeight = 14
      Items.WideStrings = (
        #1587#1608#1574#1740#1670
        #1601#1606#1583#1705
        #1580#1705
        #1570#1670#1575#1585' '#1670#1585#1582
        #1570#1574#1740#1606#1607' '#1583#1575#1582#1604
        #1586#1575#1662#1575#1587
        #1570#1574#1740#1606#1607' '#1576#1594#1604
        #1602#1575#1604#1662#1575#1602
        #1585#1575#1583#1740#1608' '#1662#1582#1588
        #1662#1585#1688#1705#1578#1608#1585
        #1576#1604#1606#1583#1711#1608
        #1705#1662#1587#1608#1604
        #1602#1601#1604' '#1608' '#1586#1606#1580#1740#1585)
      Items.WideStrings_UTF7 = (
        '+BjMGSAYmBswGhg'
        '+BkEGRgYvBqk'
        '+BiwGqQ'
        '+BiIGhgYnBjE +BoYGMQYu'
        '+BiIGJgbMBkYGRw +Bi8GJwYuBkQ'
        '+BjIGJwZ+BicGMw'
        '+BiIGJgbMBkYGRw +BigGOgZE'
        '+BkIGJwZEBn4GJwZC'
        '+BjEGJwYvBswGSA +Bn4GLgY0'
        '+Bn4GMQaYBqkGKgZIBjE'
        '+BigGRAZGBi8GrwZI'
        '+BqkGfgYzBkgGRA'
        '+BkIGQQZE +Bkg +BjIGRgYsBswGMQ')
      TabOrder = 1
    end
  end
  object Panel2: TPanel
    Left = 161
    Top = 0
    Width = 463
    Height = 272
    Align = alClient
    TabOrder = 1
    object TntShape13: TTntShape
      Left = 7
      Top = 8
      Width = 450
      Height = 257
      Brush.Color = clBtnFace
      Pen.Color = clGray
      Pen.Style = psDot
      Shape = stRoundRect
    end
    object TntLabel1: TTntLabel
      Left = 386
      Top = 32
      Width = 60
      Height = 14
      Caption = #1705#1583' '#1576#1585#1711#1607' '#1586#1585#1583':'
      Color = clBtnFace
      ParentColor = False
      Caption_UTF7 = '+BqkGLw +BigGMQavBkc +BjIGMQYv:'
    end
    object TntLabel2: TTntLabel
      Left = 325
      Top = 32
      Width = 19
      Height = 14
      Caption = #1606#1608#1593':'
      Color = clBtnFace
      ParentColor = False
      Caption_UTF7 = '+BkYGSAY5:'
    end
    object TntLabel3: TTntLabel
      Left = 218
      Top = 32
      Width = 23
      Height = 14
      Caption = #1585#1606#1711':'
      Color = clBtnFace
      ParentColor = False
      Caption_UTF7 = '+BjEGRgav:'
    end
    object TntLabel4: TTntLabel
      Left = 80
      Top = 32
      Width = 58
      Height = 14
      Caption = #1578#1575#1585#1740#1582' '#1578#1608#1602#1740#1601':'
      Color = clBtnFace
      ParentColor = False
      Caption_UTF7 = '+BioGJwYxBswGLg +BioGSAZCBswGQQ:'
    end
    object TntLabel5: TTntLabel
      Left = 382
      Top = 72
      Width = 64
      Height = 14
      Caption = #1588#1605#1575#1585#1607' '#1662#1604#1575#1705':'
      Color = clBtnFace
      ParentColor = False
      Caption_UTF7 = '+BjQGRQYnBjEGRw +Bn4GRAYnBqk:'
    end
    object TntLabel6: TTntLabel
      Left = 283
      Top = 72
      Width = 61
      Height = 14
      Caption = #1588#1605#1575#1585#1607' '#1576#1583#1606#1607':'
      Color = clBtnFace
      ParentColor = False
      Caption_UTF7 = '+BjQGRQYnBjEGRw +BigGLwZGBkc:'
    end
    object TntLabel7: TTntLabel
      Left = 177
      Top = 72
      Width = 64
      Height = 14
      Caption = #1588#1605#1575#1585#1607' '#1605#1608#1578#1608#1585':'
      Color = clBtnFace
      ParentColor = False
      Caption_UTF7 = '+BjQGRQYnBjEGRw +BkUGSAYqBkgGMQ:'
    end
    object TntLabel8: TTntLabel
      Left = 79
      Top = 72
      Width = 59
      Height = 14
      Caption = #1608#1575#1581#1583' '#1570#1608#1585#1606#1583#1607':'
      Color = clBtnFace
      ParentColor = False
      Caption_UTF7 = '+BkgGJwYtBi8 +BiIGSAYxBkYGLwZH:'
    end
    object TntLabel9: TTntLabel
      Left = 424
      Top = 112
      Width = 22
      Height = 14
      Caption = #1575#1606#1576#1575#1585':'
      Color = clBtnFace
      ParentColor = False
      Caption_UTF7 = '+BicGRgYoBicGMQ:'
    end
    object TntLabel10: TTntLabel
      Left = 316
      Top = 112
      Width = 28
      Height = 14
      Caption = #1585#1583#1740#1601':'
      Color = clBtnFace
      ParentColor = False
      Caption_UTF7 = '+BjEGLwbMBkE:'
    end
    object TntLabel13: TTntLabel
      Left = 197
      Top = 112
      Width = 44
      Height = 14
      Caption = #1606#1608#1593' '#1590#1576#1591':'
      Color = clBtnFace
      ParentColor = False
      Caption_UTF7 = '+BkYGSAY5 +BjYGKAY3:'
    end
    object TntLabel12: TTntLabel
      Left = 300
      Top = 152
      Width = 146
      Height = 14
      Caption = #1578#1608#1590#1740#1581#1575#1578' ('#1581#1583#1575#1705#1579#1585' 255 '#1581#1585#1601'):'
      Color = clBtnFace
      ParentColor = False
      Caption_UTF7 = '+BioGSAY2BswGLQYnBio (+Bi0GLwYnBqkGKwYx 255 +Bi0GMQZB):'
    end
    object TntLabel14: TTntLabel
      Left = 73
      Top = 112
      Width = 64
      Height = 14
      Caption = #1578#1575#1585#1740#1582' '#1578#1585#1582#1740#1589':'
      Color = clBtnFace
      ParentColor = False
      Caption_UTF7 = '+BioGJwYxBswGLg +BioGMQYuBswGNQ:'
    end
    object TntLabel15: TTntLabel
      Left = 64
      Top = 152
      Width = 73
      Height = 14
      Caption = #1607#1586#1740#1606#1607' '#1583#1585#1740#1575#1601#1578#1740':'
      Color = clBtnFace
      ParentColor = False
      Caption_UTF7 = '+BkcGMgbMBkYGRw +Bi8GMQbMBicGQQYqBsw:'
    end
    object Onvan: TTntLabel
      Left = 242
      Top = 16
      Width = 36
      Height = 16
      Caption = 'Onvan'
      Color = clBtnFace
      Font.Charset = ARABIC_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentColor = False
      ParentFont = False
    end
    object TntButton9: TTntButton
      Left = 21
      Top = 227
      Width = 68
      Height = 25
      Caption = #1579#1576#1578
      Default = True
      TabOrder = 18
      OnClick = TntButton9Click
      Caption_UTF7 = '+BisGKAYq'
    end
    object TntButton1: TTntButton
      Left = 21
      Top = 195
      Width = 68
      Height = 25
      Cancel = True
      Caption = #1582#1585#1608#1580
      ModalResult = 1
      TabOrder = 17
      OnClick = TntButton1Click
      Caption_UTF7 = '+Bi4GMQZIBiw'
    end
    object BargeZardCode: TTntEdit
      Left = 357
      Top = 48
      Width = 89
      Height = 22
      BevelInner = bvNone
      BevelKind = bkFlat
      BorderStyle = bsNone
      TabOrder = 0
    end
    object MotorTyp: TTntEdit
      Left = 255
      Top = 48
      Width = 89
      Height = 22
      BevelInner = bvNone
      BevelKind = bkFlat
      BorderStyle = bsNone
      TabOrder = 1
    end
    object MotorColor: TTntEdit
      Left = 152
      Top = 48
      Width = 89
      Height = 22
      BevelInner = bvNone
      BevelKind = bkFlat
      BorderStyle = bsNone
      TabOrder = 2
    end
    object ShomarePlak: TTntEdit
      Left = 357
      Top = 88
      Width = 89
      Height = 22
      BevelInner = bvNone
      BevelKind = bkFlat
      BorderStyle = bsNone
      TabOrder = 7
    end
    object ShomareBadane: TTntEdit
      Left = 255
      Top = 88
      Width = 89
      Height = 22
      BevelInner = bvNone
      BevelKind = bkFlat
      BorderStyle = bsNone
      TabOrder = 8
    end
    object ShomareMotor: TTntEdit
      Left = 152
      Top = 88
      Width = 89
      Height = 22
      BevelInner = bvNone
      BevelKind = bkFlat
      BorderStyle = bsNone
      TabOrder = 9
    end
    object NooZabtCB5: TTntComboBox
      Left = 152
      Top = 127
      Width = 89
      Height = 22
      Style = csDropDownList
      Color = clWhite
      ItemHeight = 14
      TabOrder = 13
    end
    object YeganCB1: TTntComboBox
      Left = 16
      Top = 87
      Width = 122
      Height = 22
      Style = csDropDownList
      Color = clWhite
      ItemHeight = 14
      TabOrder = 10
    end
    object AnbarCB1: TTntComboBox
      Left = 361
      Top = 127
      Width = 85
      Height = 22
      Style = csDropDownList
      Color = clWhite
      ItemHeight = 14
      TabOrder = 11
    end
    object Radif: TTntEdit
      Left = 311
      Top = 128
      Width = 33
      Height = 22
      BevelInner = bvNone
      BevelKind = bkFlat
      BorderStyle = bsNone
      MaxLength = 4
      TabOrder = 12
      OnKeyPress = RadifKeyPress
    end
    object DayED1: TTntEdit
      Left = 120
      Top = 48
      Width = 18
      Height = 20
      BevelInner = bvNone
      BevelKind = bkFlat
      BorderStyle = bsNone
      Color = clWhite
      MaxLength = 2
      TabOrder = 3
      OnKeyPress = RadifKeyPress
    end
    object MonthED1: TTntEdit
      Left = 96
      Top = 48
      Width = 18
      Height = 20
      BevelInner = bvNone
      BevelKind = bkFlat
      BorderStyle = bsNone
      Color = clWhite
      MaxLength = 2
      TabOrder = 4
      OnKeyPress = RadifKeyPress
    end
    object YearED2: TTntEdit
      Left = 56
      Top = 48
      Width = 34
      Height = 20
      BevelInner = bvNone
      BevelKind = bkFlat
      BorderStyle = bsNone
      Color = clWhite
      MaxLength = 4
      TabOrder = 5
      OnKeyPress = RadifKeyPress
    end
    object Today: TTntButton
      Left = 13
      Top = 48
      Width = 37
      Height = 20
      Caption = #1575#1605#1585#1608#1586
      TabOrder = 6
      OnClick = TodayClick
      Caption_UTF7 = '+BicGRQYxBkgGMg'
    end
    object Tozihat: TTntMemo
      Left = 152
      Top = 168
      Width = 297
      Height = 81
      BevelInner = bvNone
      BevelKind = bkFlat
      BorderStyle = bsNone
      MaxLength = 255
      TabOrder = 16
    end
    object TarikhTarkhis: TTntEdit
      Left = 48
      Top = 128
      Width = 89
      Height = 22
      BevelInner = bvNone
      BevelKind = bkFlat
      BorderStyle = bsNone
      TabOrder = 14
    end
    object HazineDaryafti: TTntEdit
      Left = 48
      Top = 168
      Width = 89
      Height = 22
      BevelInner = bvNone
      BevelKind = bkFlat
      BorderStyle = bsNone
      TabOrder = 15
    end
  end
end

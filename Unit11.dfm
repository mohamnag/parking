object ACForm: TACForm
  Left = 482
  Top = 276
  BiDiMode = bdRightToLeft
  BorderStyle = bsDialog
  Caption = #1601#1593#1575#1604' '#1587#1575#1586#1610
  ClientHeight = 289
  ClientWidth = 281
  Color = clBtnFace
  Font.Charset = ARABIC_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  ParentBiDiMode = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 14
  object TntLabel1: TTntLabel
    Left = 12
    Top = 8
    Width = 261
    Height = 42
    Caption = 
      #1578#1608#1580#1607': '#1575#1740#1606' '#1575#1608#1604#1740#1606' '#1576#1575#1585#1740' '#1575#1587#1578' '#1705#1607' '#1588#1605#1575' '#1576#1585#1606#1575#1605#1607' '#1585#1575' '#1575#1580#1585#1575' '#1605#1740' '#1705#1606#1740#1583' '#1740#1575' '#1605#1588#1705#1604#1740' ' +
      #1576#1585#1575#1740' '#1705#1583' '#1601#1593#1575#1604#1587#1575#1586#1740' '#1662#1740#1588' '#1570#1605#1583#1607'. '#1604#1591#1601#1575' '#1705#1583' '#1601#1593#1575#1604#1587#1575#1586#1740' '#1585#1575' '#1608#1575#1585#1583' '#1606#1605#1575#1740#1740#1583'.'
    Font.Charset = ARABIC_CHARSET
    Font.Color = clRed
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    WordWrap = True
    Caption_UTF7 = 
      '+BioGSAYsBkc: +BicGzAZG +BicGSAZEBswGRg +BigGJwYxBsw +BicGMwYq +' +
      'BqkGRw +BjQGRQYn +BigGMQZGBicGRQZH +BjEGJw +BicGLAYxBic +BkUGzA ' +
      '+BqkGRgbMBi8 +BswGJw +BkUGNAapBkQGzA +BigGMQYnBsw +BqkGLw +BkEGO' +
      'QYnBkQGMwYnBjIGzA +Bn4GzAY0 +BiIGRQYvBkc. +BkQGNwZBBic +BqkGLw +' +
      'BkEGOQYnBkQGMwYnBjIGzA +BjEGJw +BkgGJwYxBi8 +BkYGRQYnBswGzAYv.'
  end
  object TntLabel2: TTntLabel
    Left = 15
    Top = 104
    Width = 258
    Height = 70
    Caption = 
      #1583#1585' '#1589#1608#1585#1578#1740' '#1705#1607' '#1705#1583' '#1601#1593#1575#1604#1587#1575#1586#1740' '#1585#1575' '#1606#1583#1575#1585#1740#1583' '#1740#1575' '#1578#1594#1740#1740#1585#1740' '#1583#1585' '#1587#1582#1578' '#1575#1601#1586#1575#1585' '#1582#1608#1583' '#1583#1575#1583 +
      #1607' '#1575#1740#1583' '#1705#1583' '#1586#1740#1585' '#1585#1575' '#1576#1585#1575#1740' '#1576#1585#1606#1575#1605#1607' '#1606#1608#1740#1587' '#1576#1607' '#1570#1583#1585#1587' '#1662#1587#1578' '#1575#1604#1705#1578#1585#1608#1606#1740#1705#1740' '#1740#1575' '#1588#1605#1575#1585#1607 +
      ' '#1578#1605#1575#1587' '#1586#1740#1585' '#1575#1585#1587#1575#1604' '#1606#1605#1575#1740#1740#1583' '#1578#1575' '#1583#1585' '#1589#1608#1585#1578' '#1605#1593#1578#1576#1585' '#1576#1608#1583#1606' '#1705#1662#1740' '#1588#1605#1575' '#1575#1586' '#1606#1585#1605' '#1575#1601#1586#1575 +
      #1585' '#1576#1585#1575#1740' '#1588#1605#1575' '#1705#1583' '#1601#1593#1575#1604#1587#1575#1586#1740' '#1575#1585#1587#1575#1604' '#1588#1608#1583'.'
    Font.Charset = ARABIC_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    WordWrap = True
    Caption_UTF7 = 
      '+Bi8GMQ +BjUGSAYxBioGzA +BqkGRw +BqkGLw +BkEGOQYnBkQGMwYnBjIGzA ' +
      '+BjEGJw +BkYGLwYnBjEGzAYv +BswGJw +BioGOgbMBswGMQbM +Bi8GMQ +BjM' +
      'GLgYq +BicGQQYyBicGMQ +Bi4GSAYv +Bi8GJwYvBkc +BicGzAYv +BqkGLw +' +
      'BjIGzAYx +BjEGJw +BigGMQYnBsw +BigGMQZGBicGRQZH +BkYGSAbMBjM +Bi' +
      'gGRw +BiIGLwYxBjM +Bn4GMwYq +BicGRAapBioGMQZIBkYGzAapBsw +BswGJw' +
      ' +BjQGRQYnBjEGRw +BioGRQYnBjM +BjIGzAYx +BicGMQYzBicGRA +BkYGRQY' +
      'nBswGzAYv +BioGJw +Bi8GMQ +BjUGSAYxBio +BkUGOQYqBigGMQ +BigGSAYv' +
      'BkY +BqkGfgbM +BjQGRQYn +BicGMg +BkYGMQZF +BicGQQYyBicGMQ +BigGM' +
      'QYnBsw +BjQGRQYn +BqkGLw +BkEGOQYnBkQGMwYnBjIGzA +BicGMQYzBicGRA' +
      ' +BjQGSAYv.'
  end
  object TntLabel3: TTntLabel
    Left = 8
    Top = 232
    Width = 163
    Height = 14
    BiDiMode = bdLeftToRight
    Caption = 'E-Mail: mohamnag@gmail.com'
    ParentBiDiMode = False
  end
  object TntLabel4: TTntLabel
    Left = 8
    Top = 216
    Width = 77
    Height = 14
    BiDiMode = bdLeftToRight
    Caption = '09125579049'
    ParentBiDiMode = False
  end
  object TntButton1: TTntButton
    Left = 8
    Top = 59
    Width = 75
    Height = 25
    Caption = #1601#1593#1575#1604' '#1705#1606
    ModalResult = 1
    TabOrder = 0
    Caption_UTF7 = '+BkEGOQYnBkQ +BqkGRg'
  end
  object InputCode: TTntEdit
    Left = 148
    Top = 59
    Width = 121
    Height = 22
    BevelInner = bvNone
    BevelKind = bkFlat
    BiDiMode = bdLeftToRight
    BorderStyle = bsNone
    ParentBiDiMode = False
    TabOrder = 1
  end
  object TntPanel1: TTntPanel
    Left = 20
    Top = 93
    Width = 241
    Height = 3
    TabOrder = 2
  end
  object TntButton2: TTntButton
    Left = 8
    Top = 256
    Width = 75
    Height = 25
    Caption = #1576#1587#1740#1575#1585' '#1582#1608#1576
    ModalResult = 2
    TabOrder = 3
    Caption_UTF7 = '+BigGMwbMBicGMQ +Bi4GSAYo'
  end
  object ACCode: TTntEdit
    Left = 8
    Top = 184
    Width = 261
    Height = 22
    BevelInner = bvNone
    BevelKind = bkFlat
    BiDiMode = bdLeftToRight
    BorderStyle = bsNone
    ParentBiDiMode = False
    ReadOnly = True
    TabOrder = 4
    Text = 'ACCode'
  end
end

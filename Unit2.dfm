object Tarkhis: TTarkhis
  Left = 291
  Top = 271
  BiDiMode = bdRightToLeft
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = #1578#1585#1582#1610#1589
  ClientHeight = 258
  ClientWidth = 486
  Color = clBtnFace
  Font.Charset = ARABIC_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  ParentBiDiMode = False
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 14
  object TntShape1: TTntShape
    Left = 8
    Top = 13
    Width = 470
    Height = 140
    Brush.Color = clBtnFace
    Pen.Color = clGray
    Pen.Style = psDot
    Shape = stRoundRect
  end
  object TntLabel1: TTntLabel
    Left = 314
    Top = 196
    Width = 158
    Height = 14
    Caption = #1607#1586#1740#1606#1607#8204#1740' '#1607#1585' '#1585#1608#1586' '#1578#1608#1602#1601' '#1583#1585' '#1662#1575#1585#1705#1740#1606#1711':'
    Caption_UTF7 = 
      '+BkcGMgbMBkYGRyAMBsw +BkcGMQ +BjEGSAYy +BioGSAZCBkE +Bi8GMQ +Bn4' +
      'GJwYxBqkGzAZGBq8:'
  end
  object TntLabel2: TTntLabel
    Left = 264
    Top = 5
    Width = 183
    Height = 16
    Caption = ' '#1605#1588#1582#1589#1575#1578' '#1608#1587#1740#1604#1607#8204#1740' '#1605#1608#1585#1583' '#1578#1585#1582#1740#1589' '
    Color = clBtnFace
    Font.Charset = ARABIC_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsItalic]
    ParentColor = False
    ParentFont = False
    Caption_UTF7 = 
      ' +BkUGNAYuBjUGJwYq +BkgGMwbMBkQGRyAMBsw +BkUGSAYxBi8 +BioGMQYuBs' +
      'wGNQ '
  end
  object TntLabel3: TTntLabel
    Left = 205
    Top = 196
    Width = 19
    Height = 14
    Caption = #1585#1740#1575#1604
    Caption_UTF7 = '+BjEGzAYnBkQ'
  end
  object TntLabel4: TTntLabel
    Left = 130
    Top = 196
    Width = 54
    Height = 14
    Caption = #1607#1586#1740#1606#1607' '#1705#1604': '
    Caption_UTF7 = '+BkcGMgbMBkYGRw +BqkGRA: '
  end
  object Hazine: TTntLabel
    Left = 125
    Top = 196
    Width = 4
    Height = 14
    BiDiMode = bdRightToLeft
    ParentBiDiMode = False
  end
  object TntLabel6: TTntLabel
    Left = 302
    Top = 123
    Width = 155
    Height = 14
    Caption = #1578#1593#1583#1575#1583' '#1585#1608#1586#1607#1575#1740' '#1578#1608#1602#1601' '#1583#1585' '#1662#1575#1585#1705#1740#1606#1711': '
    Color = clBtnFace
    ParentColor = False
    Caption_UTF7 = 
      '+BioGOQYvBicGLw +BjEGSAYyBkcGJwbM +BioGSAZCBkE +Bi8GMQ +Bn4GJwYx' +
      'BqkGzAZGBq8: '
  end
  object TntLabel8: TTntLabel
    Left = 401
    Top = 32
    Width = 60
    Height = 14
    Caption = #1705#1583' '#1576#1585#1711#1607' '#1586#1585#1583':'
    Color = clBtnFace
    ParentColor = False
    Caption_UTF7 = '+BqkGLw +BigGMQavBkc +BjIGMQYv:'
  end
  object TntLabel9: TTntLabel
    Left = 307
    Top = 32
    Width = 46
    Height = 14
    Caption = #1606#1608#1593' '#1605#1608#1578#1608#1585':'
    Color = clBtnFace
    ParentColor = False
    Caption_UTF7 = '+BkYGSAY5 +BkUGSAYqBkgGMQ:'
  end
  object TntLabel10: TTntLabel
    Left = 200
    Top = 32
    Width = 50
    Height = 14
    Caption = #1585#1606#1711' '#1605#1608#1578#1608#1585':'
    Color = clBtnFace
    ParentColor = False
    Caption_UTF7 = '+BjEGRgav +BkUGSAYqBkgGMQ:'
  end
  object TntLabel11: TTntLabel
    Left = 89
    Top = 32
    Width = 58
    Height = 14
    Caption = #1578#1575#1585#1740#1582' '#1578#1608#1602#1740#1601':'
    Color = clBtnFace
    ParentColor = False
    Caption_UTF7 = '+BioGJwYxBswGLg +BioGSAZCBswGQQ:'
  end
  object TntLabel12: TTntLabel
    Left = 397
    Top = 72
    Width = 64
    Height = 14
    Caption = #1588#1605#1575#1585#1607' '#1662#1604#1575#1705':'
    Color = clBtnFace
    ParentColor = False
    Caption_UTF7 = '+BjQGRQYnBjEGRw +Bn4GRAYnBqk:'
  end
  object TntLabel13: TTntLabel
    Left = 292
    Top = 72
    Width = 61
    Height = 14
    Caption = #1588#1605#1575#1585#1607' '#1576#1583#1606#1607':'
    Color = clBtnFace
    ParentColor = False
    Caption_UTF7 = '+BjQGRQYnBjEGRw +BigGLwZGBkc:'
  end
  object TntLabel14: TTntLabel
    Left = 186
    Top = 72
    Width = 64
    Height = 14
    Caption = #1588#1605#1575#1585#1607' '#1605#1608#1578#1608#1585':'
    Color = clBtnFace
    ParentColor = False
    Caption_UTF7 = '+BjQGRQYnBjEGRw +BkUGSAYqBkgGMQ:'
  end
  object TntLabel5: TTntLabel
    Left = 408
    Top = 168
    Width = 64
    Height = 14
    Caption = #1578#1575#1585#1740#1582' '#1578#1585#1582#1740#1589':'
    Caption_UTF7 = '+BioGJwYxBswGLg +BioGMQYuBswGNQ:'
  end
  object TntButton1: TTntButton
    Left = 104
    Top = 224
    Width = 89
    Height = 25
    Caption = #1578#1585#1582#1610#1589
    ModalResult = 1
    TabOrder = 3
    OnClick = TntButton1Click
    Caption_UTF7 = '+BioGMQYuBkoGNQ'
  end
  object TntButton2: TTntButton
    Left = 8
    Top = 224
    Width = 89
    Height = 25
    Caption = ' '#1578#1585#1582#1740#1589' '#1608' '#1670#1575#1662
    Default = True
    ModalResult = 1
    TabOrder = 2
    OnClick = TntButton2Click
    Caption_UTF7 = ' +BioGMQYuBswGNQ +Bkg +BoYGJwZ+'
  end
  object Tavagof: TTntEdit
    Left = 226
    Top = 120
    Width = 73
    Height = 20
    TabStop = False
    BevelInner = bvNone
    BevelKind = bkFlat
    BorderStyle = bsNone
    ReadOnly = True
    TabOrder = 4
  end
  object Roozane: TTntEdit
    Left = 230
    Top = 193
    Width = 80
    Height = 20
    BevelInner = bvNone
    BevelKind = bkFlat
    BiDiMode = bdLeftToRight
    BorderStyle = bsNone
    ParentBiDiMode = False
    TabOrder = 0
    OnChange = RoozaneChange
    OnKeyPress = RoozaneKeyPress
  end
  object TntButton3: TTntButton
    Left = 200
    Top = 224
    Width = 89
    Height = 25
    Cancel = True
    Caption = #1604#1594#1608
    ModalResult = 2
    TabOrder = 1
    OnClick = TntButton3Click
    Caption_UTF7 = '+BkQGOgZI'
  end
  object BargeZardCode: TTntEdit
    Left = 372
    Top = 48
    Width = 89
    Height = 22
    TabStop = False
    BevelInner = bvNone
    BevelKind = bkFlat
    BorderStyle = bsNone
    ReadOnly = True
    TabOrder = 5
  end
  object MotorTyp: TTntEdit
    Left = 264
    Top = 48
    Width = 89
    Height = 22
    TabStop = False
    BevelInner = bvNone
    BevelKind = bkFlat
    BorderStyle = bsNone
    ReadOnly = True
    TabOrder = 6
  end
  object MotorColor: TTntEdit
    Left = 161
    Top = 48
    Width = 89
    Height = 22
    TabStop = False
    BevelInner = bvNone
    BevelKind = bkFlat
    BorderStyle = bsNone
    ReadOnly = True
    TabOrder = 7
  end
  object DayED1: TTntEdit
    Left = 129
    Top = 48
    Width = 18
    Height = 20
    TabStop = False
    BevelInner = bvNone
    BevelKind = bkFlat
    BorderStyle = bsNone
    Color = clWhite
    MaxLength = 2
    ReadOnly = True
    TabOrder = 8
  end
  object MonthED1: TTntEdit
    Left = 105
    Top = 48
    Width = 18
    Height = 20
    TabStop = False
    BevelInner = bvNone
    BevelKind = bkFlat
    BorderStyle = bsNone
    Color = clWhite
    MaxLength = 2
    ReadOnly = True
    TabOrder = 9
  end
  object YearED2: TTntEdit
    Left = 65
    Top = 48
    Width = 34
    Height = 20
    TabStop = False
    BevelInner = bvNone
    BevelKind = bkFlat
    BorderStyle = bsNone
    Color = clWhite
    MaxLength = 4
    ReadOnly = True
    TabOrder = 10
  end
  object ShomarePlak: TTntEdit
    Left = 372
    Top = 88
    Width = 89
    Height = 22
    TabStop = False
    BevelInner = bvNone
    BevelKind = bkFlat
    BorderStyle = bsNone
    ReadOnly = True
    TabOrder = 11
  end
  object ShomareBadane: TTntEdit
    Left = 264
    Top = 88
    Width = 89
    Height = 22
    TabStop = False
    BevelInner = bvNone
    BevelKind = bkFlat
    BorderStyle = bsNone
    ReadOnly = True
    TabOrder = 12
  end
  object ShomareMotor: TTntEdit
    Left = 161
    Top = 88
    Width = 89
    Height = 22
    TabStop = False
    BevelInner = bvNone
    BevelKind = bkFlat
    BorderStyle = bsNone
    ReadOnly = True
    TabOrder = 13
  end
  object DayED2: TTntEdit
    Left = 380
    Top = 164
    Width = 18
    Height = 20
    BevelInner = bvNone
    BevelKind = bkFlat
    BorderStyle = bsNone
    Color = clWhite
    MaxLength = 2
    TabOrder = 14
    Text = '1'
    OnChange = DayED2Change
    OnKeyPress = RoozaneKeyPress
  end
  object MonthED2: TTntEdit
    Left = 356
    Top = 164
    Width = 18
    Height = 20
    BevelInner = bvNone
    BevelKind = bkFlat
    BorderStyle = bsNone
    Color = clWhite
    MaxLength = 2
    TabOrder = 15
    Text = '3'
    OnChange = DayED2Change
    OnKeyPress = RoozaneKeyPress
  end
  object YearED3: TTntEdit
    Left = 318
    Top = 164
    Width = 32
    Height = 20
    BevelInner = bvNone
    BevelKind = bkFlat
    BorderStyle = bsNone
    Color = clWhite
    MaxLength = 4
    TabOrder = 16
    Text = '1385'
    OnChange = DayED2Change
    OnKeyPress = RoozaneKeyPress
  end
  object Today: TTntButton
    Left = 273
    Top = 164
    Width = 37
    Height = 20
    Caption = #1575#1605#1585#1608#1586
    TabOrder = 17
    OnClick = TodayClick
    Caption_UTF7 = '+BicGRQYxBkgGMg'
  end
  object TarkhisPrnMotor: TRvProject
    ProjectFile = 'Rav\TarkhisPrnMotorRav.rav'
    Left = 328
    Top = 216
  end
  object TarkhisPrnMashin: TRvProject
    ProjectFile = 'Rav\TarkhisPrnMashinRav.rav'
    Left = 368
    Top = 216
  end
end

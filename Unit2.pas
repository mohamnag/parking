unit Unit2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, TntStdCtrls, ExtCtrls, TntExtCtrls, UFarsiDate, DateUtils,
  RpDefine, RpRave, Math;

type
  TTarkhis = class(TForm)
    TntShape1: TTntShape;
    TntLabel1: TTntLabel;
    TntLabel2: TTntLabel;
    TntLabel3: TTntLabel;
    TntButton1: TTntButton;
    TntLabel4: TTntLabel;
    Hazine: TTntLabel;
    TntButton2: TTntButton;
    TntLabel6: TTntLabel;
    Tavagof: TTntEdit;
    Roozane: TTntEdit;
    TntButton3: TTntButton;
    TntLabel8: TTntLabel;
    BargeZardCode: TTntEdit;
    MotorTyp: TTntEdit;
    TntLabel9: TTntLabel;
    TntLabel10: TTntLabel;
    MotorColor: TTntEdit;
    DayED1: TTntEdit;
    TntLabel11: TTntLabel;
    MonthED1: TTntEdit;
    YearED2: TTntEdit;
    ShomarePlak: TTntEdit;
    TntLabel12: TTntLabel;
    TntLabel13: TTntLabel;
    ShomareBadane: TTntEdit;
    TntLabel14: TTntLabel;
    ShomareMotor: TTntEdit;
    TarkhisPrnMotor: TRvProject;
    TarkhisPrnMashin: TRvProject;
    DayED2: TTntEdit;
    MonthED2: TTntEdit;
    YearED3: TTntEdit;
    Today: TTntButton;
    TntLabel5: TTntLabel;
    procedure RoozaneKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure TntButton1Click(Sender: TObject);
    procedure TntButton2Click(Sender: TObject);
    procedure SetFacParams(Typ:string);
    function DarONadar(a:boolean):string;
    procedure TodayClick(Sender: TObject);
    procedure DayED2Change(Sender: TObject);
    procedure RoozaneChange(Sender: TObject);
    procedure TntButton3Click(Sender: TObject);
  private
    { Private declarations }
  public
    DialogTyp:string;
    function PaDays(stDate, enDate: string):integer;
    { Public declarations }
  end;

var
  Tarkhis: TTarkhis;

implementation

uses Unit1,DateConv;

{$R *.dfm}

procedure TTarkhis.RoozaneKeyPress(Sender: TObject; var Key: Char);
begin
if (ord(key)<48)or(ord(key)>57) then begin
  key:=chr(0);
  beep;
end;
end;

procedure TTarkhis.FormShow(Sender: TObject);
begin
today.Click;
if DialogTyp='Motor' then begin
  BargeZardCode.Text:=MainF.MotorDS.DataSet.Fields[0].AsString;
  MotorTyp.Text:=MainF.MotorDS.DataSet.Fields[1].AsString;
  MotorColor.Text:=MainF.MotorDS.DataSet.Fields[2].AsString;
  DayED1.Text:=inttostr(TFarDate.farDayOf(TFarDate.farStrToDate(
    MainF.MotorDS.DataSet.Fields[3].AsString)));
  MonthED1.Text:=inttostr(TFarDate.farMonthOf(TFarDate.farStrToDate(
  MainF.MotorDS.DataSet.Fields[3].AsString)));
  YearED2.Text:=inttostr(TFarDate.farYearOf(TFarDate.farStrToDate(
  MainF.MotorDS.DataSet.Fields[3].AsString)));
  ShomarePlak.Text:=MainF.MotorDS.DataSet.Fields[4].AsString;
  ShomareBadane.Text:=MainF.MotorDS.DataSet.Fields[5].AsString;
  ShomareMotor.Text:=MainF.MotorDS.DataSet.Fields[6].AsString;
end;

if DialogTyp='Mashin' then begin
  BargeZardCode.Text:=MainF.MashinDS.DataSet.Fields[0].AsString;
  MotorTyp.Text:=MainF.MashinDS.DataSet.Fields[1].AsString;
  MotorColor.Text:=MainF.MashinDS.DataSet.Fields[2].AsString;
  DayED1.Text:=inttostr(TFarDate.farDayOf(TFarDate.farStrToDate(
    MainF.MashinDS.DataSet.Fields[3].AsString)));
  MonthED1.Text:=inttostr(TFarDate.farMonthOf(TFarDate.farStrToDate(
    MainF.MashinDS.DataSet.Fields[3].AsString)));
  YearED2.Text:=inttostr(TFarDate.farYearOf(TFarDate.farStrToDate(
    MainF.MashinDS.DataSet.Fields[3].AsString)));
  ShomarePlak.Text:=MainF.MashinDS.DataSet.Fields[4].AsString;
  ShomareBadane.Text:=MainF.MashinDS.DataSet.Fields[5].AsString;
  ShomareMotor.Text:=MainF.MashinDS.DataSet.Fields[6].AsString;
end;

end;

procedure TTarkhis.TntButton1Click(Sender: TObject);
begin
if DialogTyp='Motor' then begin
  MainF.MotorDS.DataSet.Edit;
  MainF.MotorDS.DataSet.Fields[10].AsString:=
    YearED3.Text+'/'+MonthED2.Text+'/'+DayED2.Text;
  MainF.MotorDS.DataSet.Fields[11].AsInteger:=100*(Round(strtofloat(Roozane.Text)
                                              *strtofloat(Tavagof.Text)) div 100);
  MainF.MotorDS.DataSet.Post;
end;
if DialogTyp='Mashin' then begin
  MainF.MashinDS.DataSet.Edit;
  MainF.MashinDS.DataSet.Fields[10].AsString:=
    YearED3.Text+'/'+MonthED2.Text+'/'+DayED2.Text;
  MainF.MashinDS.DataSet.Fields[11].AsInteger:=100*(Round(strtofloat(Roozane.Text)
                                                *strtofloat(Tavagof.Text)) div 100);
  MainF.MashinDS.DataSet.Post;
end;
Roozane.Text:='';
Hazine.Caption:='';
Tarkhis.ModalResult:=MROK;
end;

procedure TTarkhis.TntButton2Click(Sender: TObject);
begin
  //for mashin

if DialogTyp='Motor' then begin
  MainF.MotorDS.DataSet.Edit;
  MainF.MotorDS.DataSet.Fields[10].AsString:=
    YearED3.Text+'/'+MonthED2.Text+'/'+DayED2.Text;
  MainF.MotorDS.DataSet.Fields[11].AsInteger:=Round(100*(strtofloat(Roozane.Text)
                                                *strtofloat(Tavagof.Text))) div 100;
  MainF.MotorDS.DataSet.Post;
  SetFacParams(DialogTyp);
  TarkhisPrnMotor.execute;
end;

if DialogTyp='Mashin' then begin
  MainF.MashinDS.DataSet.Edit;
  MainF.MashinDS.DataSet.Fields[10].AsString:=
    YearED3.Text+'/'+MonthED2.Text+'/'+DayED2.Text;
  MainF.MashinDS.DataSet.Fields[11].AsInteger:=Round(100*(strtofloat(Roozane.Text)
                                                *strtofloat(Tavagof.Text))) div 100;
  MainF.MashinDS.DataSet.Post;
  SetFacParams(DialogTyp);
  TarkhisPrnMashin.execute;
end;
Roozane.Text:='';
Hazine.Caption:='';

Tarkhis.ModalResult:=MROK;
end;

procedure TTarkhis.SetFacParams(Typ: string);
begin

if Typ='Motor' then begin
  TarkhisPrnMotor.SetParam('RoozTavagof',Tavagof.Text);
  TarkhisPrnMotor.SetParam('CodeBrgeZard',MainF.MotorDS.DataSet.Fields[0].AsString);
  TarkhisPrnMotor.SetParam('Noo',MainF.MotorDS.DataSet.Fields[1].AsString );
  TarkhisPrnMotor.SetParam('Rang',MainF.MotorDS.DataSet.Fields[2].AsString );

  TarkhisPrnMotor.SetParam('TarikhVorod',MainF.MotorDS.DataSet.Fields[3].AsString);

  TarkhisPrnMotor.SetParam('ShomarePelak',MainF.MotorDS.DataSet.Fields[4].AsString );
  TarkhisPrnMotor.SetParam('ShomareBadane',MainF.MotorDS.DataSet.Fields[5].AsString );
  TarkhisPrnMotor.SetParam('ShomareMotor',MainF.MotorDS.DataSet.Fields[6].AsString );
  TarkhisPrnMotor.SetParam('Yegan',MainF.MotorDS.DataSet.Fields[7].AsString );
  TarkhisPrnMotor.SetParam('Anbar',MainF.MotorDS.DataSet.Fields[8].AsString );
  TarkhisPrnMotor.SetParam('Radif',MainF.MotorDS.DataSet.Fields[9].AsString );

  TarkhisPrnMotor.SetParam('TarikhTarkhis',MainF.MotorDS.DataSet.Fields[10].AsString);

  TarkhisPrnMotor.SetParam('HazineDaryafti',MainF.MotorDS.DataSet.Fields[11].AsString );
  TarkhisPrnMotor.SetParam('NooeZabt',MainF.MotorDS.DataSet.Fields[12].AsString );
  TarkhisPrnMotor.SetParam('VSJak',DarONadar(MainF.MotorDS.DataSet.Fields[13].AsBoolean ));
  TarkhisPrnMotor.SetParam('VSGabZanjir',DarONadar(MainF.MotorDS.DataSet.Fields[14].AsBoolean));
  TarkhisPrnMotor.SetParam('VSRecabJelo',DarONadar(MainF.MotorDS.DataSet.Fields[15].AsBoolean));
  TarkhisPrnMotor.SetParam('VSHendel',DarONadar(MainF.MotorDS.DataSet.Fields[16].AsBoolean));
  TarkhisPrnMotor.SetParam('VSAchar',DarONadar(MainF.MotorDS.DataSet.Fields[17].AsBoolean));
  TarkhisPrnMotor.SetParam('VSBatri',DarONadar(MainF.MotorDS.DataSet.Fields[18].AsBoolean));
  TarkhisPrnMotor.SetParam('VSBaak',DarONadar(MainF.MotorDS.DataSet.Fields[19].AsBoolean));
  TarkhisPrnMotor.SetParam('VSGabBagalDotarf',DarONadar(MainF.MotorDS.DataSet.Fields[20].AsBoolean));
  TarkhisPrnMotor.SetParam('VSDasteTormoz',DarONadar(MainF.MotorDS.DataSet.Fields[21].AsBoolean));
  TarkhisPrnMotor.SetParam('VSDasteKelaj',DarONadar(MainF.MotorDS.DataSet.Fields[22].AsBoolean));
  TarkhisPrnMotor.SetParam('VSKaseKilometr',DarONadar(MainF.MotorDS.DataSet.Fields[23].AsBoolean));
  TarkhisPrnMotor.SetParam('VSKaseDoreMotor',DarONadar(MainF.MotorDS.DataSet.Fields[24].AsBoolean));
  TarkhisPrnMotor.SetParam('VSCherageJelo',DarONadar(MainF.MotorDS.DataSet.Fields[25].AsBoolean));
  TarkhisPrnMotor.SetParam('VSCherageAgab',DarONadar(MainF.MotorDS.DataSet.Fields[26].AsBoolean));
  TarkhisPrnMotor.SetParam('VSCherageRahnamaChap',DarONadar(MainF.MotorDS.DataSet.Fields[27].AsBoolean));
  TarkhisPrnMotor.SetParam('VSCherageRahnamaRast',DarONadar(MainF.MotorDS.DataSet.Fields[28].AsBoolean));
  TarkhisPrnMotor.SetParam('VSAineh',DarONadar(MainF.MotorDS.DataSet.Fields[29].AsBoolean));
  TarkhisPrnMotor.SetParam('VSToshak',DarONadar(MainF.MotorDS.DataSet.Fields[30].AsBoolean));
  TarkhisPrnMotor.SetParam('VSArmeBadaneh',DarONadar(MainF.MotorDS.DataSet.Fields[31].AsBoolean));
  TarkhisPrnMotor.SetParam('VSBoog',DarONadar(MainF.MotorDS.DataSet.Fields[32].AsBoolean));
  TarkhisPrnMotor.SetParam('VSGofVaZanjir',DarONadar(MainF.MotorDS.DataSet.Fields[33].AsBoolean));
  TarkhisPrnMotor.SetParam('VSTalg',DarONadar(MainF.MotorDS.DataSet.Fields[34].AsBoolean));
  TarkhisPrnMotor.SetParam('VSSwitch',DarONadar(MainF.MotorDS.DataSet.Fields[35].AsBoolean));
  TarkhisPrnMotor.SetParam('Tozihat',MainF.MotorDS.DataSet.Fields[36].AsString);
end;

if Typ='Mashin' then begin
  TarkhisPrnMashin.SetParam('RoozTavagof',Tavagof.Text);
  TarkhisPrnMashin.SetParam('CodeBrgeZard',MainF.MashinDS.DataSet.Fields[0].AsString);
  TarkhisPrnMashin.SetParam('Noo',MainF.MashinDS.DataSet.Fields[1].AsString );
  TarkhisPrnMashin.SetParam('Rang',MainF.MashinDS.DataSet.Fields[2].AsString );

  TarkhisPrnMashin.SetParam('TarikhVorod',MainF.MashinDS.DataSet.Fields[3].AsString);

  TarkhisPrnMashin.SetParam('ShomarePelak',MainF.MashinDS.DataSet.Fields[4].AsString );
  TarkhisPrnMashin.SetParam('ShomareBadane',MainF.MashinDS.DataSet.Fields[5].AsString );
  TarkhisPrnMashin.SetParam('ShomareMotor',MainF.MashinDS.DataSet.Fields[6].AsString );
  TarkhisPrnMashin.SetParam('Yegan',MainF.MashinDS.DataSet.Fields[7].AsString );
  TarkhisPrnMashin.SetParam('Anbar',MainF.MashinDS.DataSet.Fields[8].AsString );
  TarkhisPrnMashin.SetParam('Radif',MainF.MashinDS.DataSet.Fields[9].AsString );

  TarkhisPrnMashin.SetParam('TarikhTarkhis',MainF.MashinDS.DataSet.Fields[10].AsString);

  TarkhisPrnMashin.SetParam('HazineDaryafti',MainF.MashinDS.DataSet.Fields[11].AsString );
  TarkhisPrnMashin.SetParam('NooeZabt',MainF.MashinDS.DataSet.Fields[12].AsString );
  TarkhisPrnMashin.SetParam('VSSwitch',DarONadar(MainF.MashinDS.DataSet.Fields[13].AsBoolean));
  TarkhisPrnMashin.SetParam('VSFandak',DarONadar(MainF.MashinDS.DataSet.Fields[14].AsBoolean));
  TarkhisPrnMashin.SetParam('VSJak',DarONadar(MainF.MashinDS.DataSet.Fields[15].AsBoolean));
  TarkhisPrnMashin.SetParam('VSAchaCharkh',DarONadar(MainF.MashinDS.DataSet.Fields[16].AsBoolean));
  TarkhisPrnMashin.SetParam('VSAinehDakhel',DarONadar(MainF.MashinDS.DataSet.Fields[17].AsBoolean));
  TarkhisPrnMashin.SetParam('VSZapas',DarONadar(MainF.MashinDS.DataSet.Fields[18].AsBoolean));
  TarkhisPrnMashin.SetParam('VSAinehBagal',DarONadar(MainF.MashinDS.DataSet.Fields[19].AsBoolean));
  TarkhisPrnMashin.SetParam('VSGalpagh',DarONadar(MainF.MashinDS.DataSet.Fields[20].AsBoolean));
  TarkhisPrnMashin.SetParam('VSRadioPakhsh',DarONadar(MainF.MashinDS.DataSet.Fields[21].AsBoolean));
  TarkhisPrnMashin.SetParam('VSProjektor',DarONadar(MainF.MashinDS.DataSet.Fields[22].AsBoolean));
  TarkhisPrnMashin.SetParam('VSBolandgoo',DarONadar(MainF.MashinDS.DataSet.Fields[23].AsBoolean));
  TarkhisPrnMashin.SetParam('VSCapsol',DarONadar(MainF.MashinDS.DataSet.Fields[24].AsBoolean));
  TarkhisPrnMashin.SetParam('VSGoflVaZanjir',DarONadar(MainF.MashinDS.DataSet.Fields[25].AsBoolean));
  TarkhisPrnMashin.SetParam('Tozihat',DarONadar(MainF.MashinDS.DataSet.Fields[26].AsBoolean));
end;
end;

function TTarkhis.DarONadar(a: boolean): string;
begin
if a then
  result:='����'
else
  result:='�����';

end;

procedure TTarkhis.TodayClick(Sender: TObject);
begin
  DayED2.Text:=inttostr(dayOf(TFarDate.MiladyToShamsi(now)));
  MonthED2.Text:=inttostr(MonthOf(TFarDate.MiladyToShamsi(now)));
  YearED3.Text:=inttostr(YearOf(TFarDate.MiladyToShamsi(now)));

end;

function TTarkhis.PaDays(stDate, enDate: string): integer;
var
a,b:double;
begin

a :=(TFarDate.farStrToDate(stDate));
b :=(TFarDate.farStrToDate(enDate));
if a>=b then
  result:=round(a-b)+1
else
  result:=round(b-a)+1;

end;

procedure TTarkhis.DayED2Change(Sender: TObject);
begin

if DialogTyp='Motor' then begin
  Tavagof.Text:=inttostr(PaDays(YearED3.Text+'/'+MonthED2.Text+'/'+DayED2.Text
      ,MainF.MotorDS.DataSet.Fields[3].AsString ));

end;

if DialogTyp='Mashin' then begin
  Tavagof.Text:=inttostr(PaDays(YearED3.Text+'/'+MonthED2.Text+'/'+DayED2.Text
      ,MainF.MashinDS.DataSet.Fields[3].asstring ));
end;
end;

procedure TTarkhis.RoozaneChange(Sender: TObject);
begin

if (Roozane.Text<>'')and(Tavagof.Text<>'') then
  Hazine.Caption:=inttostr(Round(100*(strtofloat(Roozane.Text)*strtofloat(Tavagof.Text))) div 100)+'����';

end;

procedure TTarkhis.TntButton3Click(Sender: TObject);
begin
Roozane.Text:='';
Hazine.Caption:='';

end;

end.

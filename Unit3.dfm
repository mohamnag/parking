object ChkPassF: TChkPassF
  Left = 350
  Top = 441
  BiDiMode = bdRightToLeft
  BorderStyle = bsDialog
  Caption = #1583#1585#1610#1575#1601#1578' '#1585#1605#1586
  ClientHeight = 162
  ClientWidth = 289
  Color = clBtnFace
  Font.Charset = ARABIC_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  ParentBiDiMode = False
  Position = poDesktopCenter
  OnHide = FormHide
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 14
  object TntLabel1: TTntLabel
    Left = 66
    Top = 8
    Width = 213
    Height = 14
    Caption = #1576#1585#1575#1740' '#1575#1606#1580#1575#1605' '#1593#1605#1604#1740#1575#1578' '#1586#1740#1585' '#1606#1740#1575#1586' '#1576#1607' '#1608#1585#1608#1583' '#1585#1605#1586' '#1583#1575#1585#1740#1583':'
    Caption_UTF7 = 
      '+BigGMQYnBsw +BicGRgYsBicGRQ +BjkGRQZEBswGJwYq +BjIGzAYx +BkYGzA' +
      'YnBjI +BigGRw +BkgGMQZIBi8 +BjEGRQYy +Bi8GJwYxBswGLw:'
  end
  object TntLabel2: TTntLabel
    Left = 260
    Top = 99
    Width = 19
    Height = 14
    Caption = #1585#1605#1586':'
    Caption_UTF7 = '+BjEGRQYy:'
  end
  object MsgL: TTntMemo
    Left = 8
    Top = 32
    Width = 273
    Height = 57
    TabStop = False
    BevelInner = bvNone
    BevelKind = bkFlat
    BorderStyle = bsNone
    Color = clBtnFace
    Lines.WideStrings = (
      'MsgL')
    ReadOnly = True
    TabOrder = 0
  end
  object TntButton1: TTntButton
    Left = 8
    Top = 128
    Width = 73
    Height = 25
    Caption = #1576#1587#1610#1575#1585' '#1582#1608#1576
    Default = True
    ModalResult = 1
    TabOrder = 3
    Caption_UTF7 = '+BigGMwZKBicGMQ +Bi4GSAYo'
  end
  object PassText: TTntEdit
    Left = 8
    Top = 96
    Width = 241
    Height = 22
    TabOrder = 1
    PasswordCharW = '*'
  end
  object TntButton2: TTntButton
    Left = 88
    Top = 128
    Width = 75
    Height = 25
    Cancel = True
    Caption = #1604#1594#1608
    ModalResult = 2
    TabOrder = 2
    Caption_UTF7 = '+BkQGOgZI'
  end
end

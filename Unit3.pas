unit Unit3;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, TntStdCtrls;

type
  TChkPassF = class(TForm)
    TntLabel1: TTntLabel;
    MsgL: TTntMemo;
    TntButton1: TTntButton;
    PassText: TTntEdit;
    TntButton2: TTntButton;
    TntLabel2: TTntLabel;
    procedure FormShow(Sender: TObject);
    procedure FormHide(Sender: TObject);
  private
    { Private declarations }
  public
    Pass:string;
    { Public declarations }
  end;

var
  ChkPassF: TChkPassF;

implementation

{$R *.dfm}

procedure TChkPassF.FormShow(Sender: TObject);
begin
PassText.SetFocus;
end;

procedure TChkPassF.FormHide(Sender: TObject);
begin
Pass:=PassText.Text;
PassText.Text:='';
end;

end.

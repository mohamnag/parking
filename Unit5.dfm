object MMessageF: TMMessageF
  Left = 345
  Top = 201
  BiDiMode = bdRightToLeft
  BorderStyle = bsDialog
  Caption = 'MMessageF'
  ClientHeight = 114
  ClientWidth = 342
  Color = clBtnFace
  Font.Charset = ARABIC_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  ParentBiDiMode = False
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 14
  object MMsgText: TTntLabel
    Left = 0
    Top = 8
    Width = 56
    Height = 14
    Alignment = taCenter
    Caption = 'MMsgText'
    WordWrap = True
  end
  object Btn2: TTntButton
    Left = 96
    Top = 80
    Width = 75
    Height = 25
    Caption = 'Btn2'
    ModalResult = 7
    TabOrder = 0
  end
  object Btn1: TTntButton
    Left = 176
    Top = 80
    Width = 75
    Height = 25
    Caption = 'Btn1'
    ModalResult = 6
    TabOrder = 1
  end
end

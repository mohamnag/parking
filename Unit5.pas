unit Unit5;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, TntStdCtrls;

type
  TMMessageF = class(TForm)
    MMsgText: TTntLabel;
    Btn2: TTntButton;
    Btn1: TTntButton;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    function Mmessagebox(Msg, Cap: string; Typ: Integer): integer;
    { Public declarations }
  end;

var
  MMessageF: TMMessageF;

implementation

{$R *.dfm}

procedure TMMessageF.FormShow(Sender: TObject);
begin
MMsgText.Width:=313;
MMsgText.Height:=81;
MMsgText.Left:=16;
MMsgText.Top:=16;
end;

function TMMessageF.Mmessagebox(Msg, Cap: string; Typ: Integer): integer;
var
a:integer;
begin
MMessageF.Caption:=Cap;
MMessageF.MMsgText.Caption:=Msg;
Result:=0;
if Typ=MB_YesNo then begin
    MMessageF.Btn1.Visible:=true;
    MMessageF.Btn1.Caption:='���';
    MMessageF.Btn2.Visible:=true;
    MMessageF.Btn2.Caption:='���';
    result:=IDNO;
    if MMessageF.ShowModal=MRYES then result:=IDYES;
  end
  else if Typ=MB_OkCancel then begin
    MMessageF.Btn1.Visible:=true;
    MMessageF.Btn1.Caption:='����� ���';
    MMessageF.Btn2.Visible:=true;
    MMessageF.Btn2.Caption:='���';
    result:=IDOK;
    if MMessageF.ShowModal=MRNO then result:=IDCANCEL;
  end
  else if Typ=MB_Ok then begin
    a:=MMessageF.Btn1.Left;
    MMessageF.Btn1.Left:=(MMessageF.Width div 2)-(MMessageF.Btn1.Width div 2);
    MMessageF.Btn1.Visible:=true;
    MMessageF.Btn1.Caption:='����� ���';
    MMessageF.Btn2.Visible:=false;
    if MMessageF.ShowModal=MRYES then result:=IDOK;
    MMessageF.Btn1.Left:=a;
  end;
end;

end.

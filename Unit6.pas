﻿unit Unit6;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, TntStdCtrls, ExtCtrls, TntExtCtrls, jpeg, IdeFileLock, md5
  ,registry, LMDCustomComponent, LMDOneInstance;

var demo : Boolean = true;

type
  TStartF = class(TForm)
    TntImage1: TTntImage;
    Timer1: TTimer;
    procedure Timer1Timer(Sender: TObject);
    procedure LMDOneInstance1Custom(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function ChkAcc():boolean;
    function ChkDemo():boolean;
  end;

var
  StartF: TStartF;

implementation

uses Unit1, Unit11;

{$R *.dfm}
    const Mag:string='æÞÊ äÑã ÇÝÒÇÑ ÊãÇã ÔÏå¡ ÈÇ ÈÑäÇãå äæíÓ ÊãÇÓ ÈíÑíÏ.';

function TStartF.ChkAcc():boolean;
var
Reg:Tregistry;
label rr;

begin

reg:=tregistry.Create;
try
  Reg.RootKey:= HKEY_LOCAL_MACHINE;
  Reg.OpenKey('SOFTWARE\IVS\MN\Parking', True);
  if md5print(MD5String(GetACFromID(GetFileAndSystemID(paramstr(0)))))
     <>Reg.ReadString('Acc') then begin
    ACForm.ACCode.Text:=GetFileAndSystemID(paramstr(0));
    rr:
    if ACForm.ShowModal=mrCancel then begin
      if Mainf.Mmessagebox('ÈÏæä ÝÚÇá ˜ÑÏä äÑã ÇÝÒÇÑ ÇÏÇãå ÇÌÑÇ ãã˜ä äíÓÊ. ÂíÇ ÎÇÑÌ ãí ÔæíÏ¿','ÎØÇ',MB_YESNO)
          =MRYES then begin
        result:=false;
        exit;
      end;
      goto rr;
    end;
    if ACForm.InputCode.text=GetACFromID(GetFileAndSystemID(paramstr(0))) then begin
      reg.WriteString('Acc',md5print(MD5String(GetACFromID(GetFileAndSystemID(paramstr(0))))));
    end
    else begin
      if Mainf.Mmessagebox('˜Ï æÇÑÏ ÔÏå ÕÍíÍ äíÓÊ. ÂíÇ ãÌÏÏÇ ÊáÇÔ ãí ˜äíÏ¿','ÎØÇ',MB_YESNO)
          =MRNO then begin
        result:=false;
        exit;
      end;
      goto rr;
    end;
  end;
finally
  Reg.Free;
end;
result:=true;
end;

procedure TStartF.Timer1Timer(Sender: TObject);
label pre;
var
ProAdd:String;
begin
Timer1.Enabled:=false;

{if not ChkAcc() then begin
  application.Terminate;
  exit;
end;}

pre:
if not(MainF.ChkPass('æÑæÏ Èå ÈÑäÇãå')) then begin
  if (MainF.Mmessagebox('ÑãÒ ÚÈæÑ ÇÔÊÈÇå ÇÓÊ¡ æÑæÏ Èå ÈÑäÇãå ãã˜ä äíÓÊ.'+chr(13)
    +'ÈÑÇí ÊáÇÔ ãÌÏÏ ÎíÑ ÑÇ ÇäÊÎÇÈ äãÇííÏ.'+chr(13)+'ÂíÇ ÇÒ ÈÑäÇãå ÎÇÑÌ ãí ÔæíÏ¿'
    ,'ÑãÒ ÇÔÊÈÇå',MB_YESNO)=IDYES) then begin
      application.Terminate;
      exit;
    end;
goto pre;
end;

MainF.AnbarNameFileAdd:='ANNF.Dat';
MainF.AjansNameFileAdd:='AJNF.Dat';
MainF.YeganNameFileAdd:='YENF.dat';
MainF.YearNameFileAdd:='YNFA.dat';
MainF.AddNooZabtFileAdd:='NZ.dat';
MainF.MamoreBargeZardFileAdd:='MBZ.dat';

ProAdd:=extractFileDir(paramstr(0))+'\';

MainF.AjansNameFileAdd:=ProAdd+MainF.AjansNameFileAdd;
MainF.YeganNameFileAdd:=ProAdd+MainF.YeganNameFileAdd;
MainF.AnbarNameFileAdd:=ProAdd+MainF.AnbarNameFileAdd;
MainF.AddNooZabtFileAdd:=ProAdd+MainF.AddNooZabtFileAdd;
MainF.MamoreBargeZardFileAdd:=ProAdd+MainF.MamoreBargeZardFileAdd;

if not(fileexists(MainF.MamoreBargeZardFileAdd)) then begin
  MainF.Mmessagebox('ÊÇ ˜äæä äÇã ÇÝÓÑ ÊÍæíá íÑäÏå ÈÑ ÒÑÏ æÇÑÏ äÔÏå¡ ÈÑÇí æÇÑÏ ˜ÑÏä äæÚ ÖÈØ Èå ÈÎÔ ÊäÙíãÇÊ ãÑÇ ÌÚå äãÇííÏ.','åÔÏÇÑ',MB_OK);
end
else begin
  MainF.FileToCBs('MamoreBargeZard',MainF.MamoreBargeZardFileAdd);
end;


if not(fileexists(MainF.AddNooZabtFileAdd)) then begin
  MainF.Mmessagebox('ÊÇ ˜äæä åí äæÚ ÖÈØí æÇÑÏ äÔÏå¡ ÈÑÇí æÇÑÏ ˜ÑÏä äæÚ ÖÈØ Èå ÈÎÔ ÊäÙíãÇÊ ãÑÇ ÌÚå äãÇííÏ.','åÔÏÇÑ',MB_OK);
end
else begin
  MainF.FileToCBs('NooZabtCB',MainF.AddNooZabtFileAdd);
end;


if not(fileexists(MainF.AjansNameFileAdd)) then begin
  //first run -> Tanzimat For Ajans
  MainF.Mmessagebox('ÊÇ ˜äæä åí äÇã ÂŽÇäÓí æÇÑÏ äÔÏå¡ ÈÑÇí ÊÚííä äÇã ÂŽÇäÓ åÇ Èå ÈÎÔ ÊäØíãÇÊ ãÑÇÌÚå äãÇííÏ','åÔÏÇÑ',MB_OK);
end
else begin
  MainF.FileToCBs('AjansCB',MainF.AjansNameFileAdd);
end;

if not(fileexists(MainF.YeganNameFileAdd)) then begin
  //first run -> Tanzimat For Yegan
  MainF.Mmessagebox('ÊÇ ˜äæä åí äÇã æÇÍÏ ÊÍæíá ÏåäÏå Çíí æÇÑÏ äÔÏå¡ ÈÑÇí ÊÚííä äÇã æÇÍÏ åÇí ÂæÑäÏå Èå ÈÎÔ ÊäØíãÇÊ ãÑÇÌÚå äãÇííÏ.','åÔÏÇÑ',MB_OK);
end
else begin
  MainF.FileToCBs('YeganCB',MainF.YeganNameFileAdd);
end;

if not(fileexists(MainF.AnbarNameFileAdd)) then begin
  //first run -> Tanzimat For Anbar
  MainF.Mmessagebox('ÊÇ ˜äæä äÇã åí ÇäÈÇÑí æÇÑÏ äÔÏå¡ ÈÑÇí ÊÚííä äÇã ÇäÈÇÑåÇ Èå ÈÎÔ ÊäÙíãÇÊ ãÑÇÌÚå äãÇííÏ.','åÔÏÇÑ',MB_OK);
end
else begin
  MainF.FileToCBs('AnbarCB',MainF.AnbarNameFileAdd);
end;

if fileexists(MainF.YearNameFileAdd)then
  MainF.FileToCBs('YearCB',MainF.YearNameFileAdd);

MainF.MotorDBTable.Active:=true;
MainF.MashinDBTable.Active:=true;
MainF.AjansDBTable.Active:=true;
MainF.MotorGozDBTable.Active:=true;
MainF.MashinGozDBTable.Active:=true;
MainF.TahvileBaregZardDB.Open;

MainF.InOutBut.Click;
StartF.Hide;
MainF.Show;
end;

function TStartF.ChkDemo: boolean;
begin

  if demo and ( (MainF.MotorDBTable.RecordCount > 10) or
      (MainF.MashinDBTable.RecordCount > 10) ) then
  begin
    ShowMessage('این نسخه آزمایشی است و بیش از 10 رکورد نمی توانید وارد نمایید.');
    Result := False
  end
  else
    Result := True;
end;

procedure TStartF.LMDOneInstance1Custom(Sender: TObject);
begin
ShowMessage('Parking 1 is already running!'+#13+'You can''t run another instance.');
//application.Terminate;
end;

end.

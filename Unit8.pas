unit Unit8;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, TntExtCtrls, StdCtrls, TntStdCtrls, UFarsiDate, DateUtils;

type
  TTahvilBargeZardF = class(TForm)
    TntLabel1: TTntLabel;
    BargeZardCode: TTntEdit;
    DayED1: TTntEdit;
    MonthED1: TTntEdit;
    YearED2: TTntEdit;
    Today: TTntButton;
    TntButton1: TTntButton;
    TntButton9: TTntButton;
    TntShape13: TTntShape;
    TntLabel3: TTntLabel;
    TntLabel8: TTntLabel;
    Vasileh: TTntComboBox;
    TntLabel9: TTntLabel;
    MamoreBargeZardCB3: TTntComboBox;
    procedure TntButton1Click(Sender: TObject);
    procedure TntButton9Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure TodayClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  TahvilBargeZardF: TTahvilBargeZardF;

implementation

uses Unit1;

{$R *.dfm}

procedure TTahvilBargeZardF.TntButton1Click(Sender: TObject);
begin
BargeZardCode.Text:='';
MamoreBargeZardCB3.Text:='';
Vasileh.ItemIndex:=0;
TahvilBargeZardF.ModalResult:=MrCancel;
end;

procedure TTahvilBargeZardF.TntButton9Click(Sender: TObject);
begin
if Vasileh.ItemIndex=0 then begin
  if not (MainF.MotorDBTable.locate('CodeBrgeZard',BargeZardCode.Text,[])) then begin
    MainF.Mmessagebox('�� �ѐ� ��� ���� ��� �� ������� ����� ���ʡ �� ���� �� ���� ������.','���',MB_OK);
    exit;
  end;
  if MainF.TahvileBaregZardDB.Locate('CodeBrgeZard',BargeZardCode.Text,[]) then begin
    MainF.Mmessagebox('��� �ѐ� ��� ���� ����� ���� ���.','���',MB_OK);
    exit;
  end;

  MainF.TahvileBaregZardDB.AppendRecord([
    BargeZardCode.Text,
    MainF.MotorDBTable.Fields[3].AsString,
    MainF.MotorDBTable.Fields[4].AsString,
    MainF.MotorDBTable.Fields[5].AsString,
    MainF.MotorDBTable.Fields[6].AsString,
    MainF.MotorDBTable.Fields[12].AsString,
    MamoreBargeZardCB3.Text,
    strtodate(YearED2.Text+'/'+MonthED1.Text+'/'+DayED1.Text),
    Vasileh.Text
    ]);
end
else begin
  if not MainF.MashinDBTable.locate('CodeBrgeZard',BargeZardCode.Text,[]) then begin
    MainF.Mmessagebox('�� �ѐ� ��� ���� ��� �� ����� �� ����� ���ʡ �� ���� �� ���� ������.','���',MB_OK);
    exit;
  end;
  if MainF.TahvileBaregZardDB.Locate('CodeBrgeZard',BargeZardCode.Text,[]) then begin
    MainF.Mmessagebox('��� �ѐ� ��� ���� ����� ���� ���.','���',MB_OK);
    exit;
  end;

  MainF.TahvileBaregZardDB.AppendRecord([
    BargeZardCode.Text,
    MainF.MashinDBTable.Fields[3].AsString,
    MainF.MashinDBTable.Fields[4].AsString,
    MainF.MashinDBTable.Fields[5].AsString,
    MainF.MashinDBTable.Fields[6].AsString,
    MainF.MashinDBTable.Fields[12].AsString,
    MamoreBargeZardCB3.Text,
    strtodate(YearED2.Text+'/'+MonthED1.Text+'/'+DayED1.Text),
    Vasileh.Text
    ]);
end;

BargeZardCode.Text:='';
MamoreBargeZardCB3.Text:='';
Vasileh.ItemIndex:=0;
Today.Click;
BargeZardCode.SetFocus;
end;

procedure TTahvilBargeZardF.FormShow(Sender: TObject);
begin
BargeZardCode.Text:='';
MamoreBargeZardCB3.Text:='';
Vasileh.ItemIndex:=0;
Today.Click;
BargeZardCode.SetFocus;
end;

procedure TTahvilBargeZardF.TodayClick(Sender: TObject);
begin
  DayED1.Text:=inttostr(dayOf(TFarDate.MiladyToShamsi(now)));
  MonthED1.Text:=inttostr(MonthOf(TFarDate.MiladyToShamsi(now)));
  YearED2.Text:=inttostr(YearOf(TFarDate.MiladyToShamsi(now)));

end;

end.

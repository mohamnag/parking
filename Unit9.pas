unit Unit9;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, TntStdCtrls, ExtCtrls, TntExtCtrls, UFarsiDate, DateUtils;

type
  TNewMotorOfAjans = class(TForm)
    TntShape13: TTntShape;
    TntButton9: TTntButton;
    Today: TTntButton;
    YearED2: TTntEdit;
    MonthED1: TTntEdit;
    DayED1: TTntEdit;
    TntLabel19: TTntLabel;
    TntLabel33: TTntLabel;
    Mablag: TTntEdit;
    TntLabel34: TTntLabel;
    YeganCB3: TTntComboBox;
    TntLabel32: TTntLabel;
    TntLabel31: TTntLabel;
    MotorNo: TTntEdit;
    AjansCB2: TTntComboBox;
    TntLabel30: TTntLabel;
    TntButton1: TTntButton;
    procedure TodayClick(Sender: TObject);
    procedure MotorNoKeyPress(Sender: TObject; var Key: Char);
    procedure TntButton1Click(Sender: TObject);
    procedure TntButton9Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
  private
    procedure clear();
    { Private declarations }
  public
    { Public declarations }
  end;

var
  NewMotorOfAjans: TNewMotorOfAjans;

implementation

uses Unit1;

{$R *.dfm}

procedure TNewMotorOfAjans.TodayClick(Sender: TObject);
begin
DayED1.Text:=inttostr(dayOf(TFarDate.MiladyToShamsi(now)));
MonthED1.Text:=inttostr(MonthOf(TFarDate.MiladyToShamsi(now)));
YearED2.Text:=inttostr(TFarDate.farYearOf(TFarDate.MiladyToShamsi(now)));

end;

procedure TNewMotorOfAjans.MotorNoKeyPress(Sender: TObject;
  var Key: Char);
begin
if (ord(key)<48)or(ord(key)>57) then begin
  key:=chr(0);
  beep;
end;
if MotorNo.Text<>'' then
  if strtoint(MotorNo.Text)>3276 then begin
    key:=chr(0);
    MainF.Mmessagebox('����� ����� ������ ��ѐ�� �� �� ���� ���.','���',MB_OK);
  end;
end;

procedure TNewMotorOfAjans.TntButton1Click(Sender: TObject);
begin
clear;
end;

procedure TNewMotorOfAjans.TntButton9Click(Sender: TObject);
begin
if AjansCB2.Text='' then begin
  MainF.Mmessagebox('��� ��� ��� ����� ���� ���ϡ ǐ� �� ���� ���� ����� ���� ��� �� ��� ������� ������ ������.','���',MB_OK);
  exit;
end;
if YeganCB3.Text='' then begin
  MainF.Mmessagebox('��� ���� ������ ��� ���� ���� ���ϡ ǐ� �� ���� ����� ����� ���� ��� �� ��� ������� ������ ������.','���',MB_OK);
  exit;
end;
if (MotorNo.Text='')or(Mablag.Text='')or(YearED2.Text='')or
    (MonthED1.Text='')or(DayED1.Text='') then begin
  MainF.Mmessagebox('�� �� ������� ����� ����ѡ ���� �� ����� ���� ��� � ��� ј��� ��� ����.','���',MB_OK);
  exit;
end;
MainF.AjansDS.DataSet.AppendRecord([AjansCB2.Text
  ,strtodate(YearED2.Text+'/'+MonthED1.Text+'/'+DayED1.Text)
  ,strtoint(MotorNo.Text),YeganCB3.Text,strtoint(Mablag.Text)]);

if not Mainf.ChkExistInCBs('YearCB',YearED2.Text) then begin
  MainF.AddCBs('YearCB',YearED2.Text);
  MainF.YearCB3.Items.SaveToFile(MainF.YearNameFileAdd);
end;
AjansCB2.ItemIndex:=0;
MotorNo.Text:='';
YeganCB3.ItemIndex:=0;
Mablag.Text:='';
Today.Click;
AjansCB2.SetFocus;
end;

procedure TNewMotorOfAjans.clear;
begin
AjansCB2.ItemIndex:=0;
MotorNo.Text:='';
YeganCB3.ItemIndex:=0;
Mablag.Text:='';
Today.Click;
ModalResult:=mrOk;
end;

procedure TNewMotorOfAjans.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
clear;
end;

procedure TNewMotorOfAjans.FormShow(Sender: TObject);
begin
AjansCB2.ItemIndex:=0;
MotorNo.Text:='';
YeganCB3.ItemIndex:=0;
Mablag.Text:='';
Today.Click;
end;

end.
